REM windows package

REM package
call mvn clean install -Dmaven.test.skip=true

REM del jeegot-dist
rmdir .dist /s /q

REM create jeegot-dist
mkdir .dist
mkdir .dist\plugins
mkdir .dist\upload
mkdir .dist\htmls

REM copy main program
xcopy jeegot-web\target\jeegot-web-*-exec.jar .dist /s /i
xcopy jeegot-web\src\main\resources\application-prod.properties .dist /s

REM copy plugins
xcopy plugins\jeegot-cms-plugin\target\*.jar .dist\plugins /s
xcopy plugins\jeegot-mall-plugin\target\*.jar .dist\plugins /s

REM copy htmls
xcopy jeegot-web\src\main\resources\htmls .dist\htmls /s /i

xcopy jeegot-web\start.bat .dist /s
xcopy jeegot-web\start.sh .dist /s

cd .dist

REM run main
rename jeegot-web-*-exec.jar jeegot-start.jar

