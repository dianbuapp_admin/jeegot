#!/bin/sh

# linux、mac上打包的脚本
mvn clean install -Dmaven.test.skip=true

# del jeegot-dist
rm -rf dist

# create jeegot-dist
mkdir dist
mkdir dist/plugins
mkdir dist/upload
mkdir dist/htmls

# copy main program and config
cp jeegot-web/target/jeegot-web-*-exec.jar dist
cp jeegot-web/src/main/resources/application-prod.properties dist

# copy plugins
cp plugins/jeegot-cms-plugin/target/*.jar dist/plugins
cp plugins/jeegot-mall-plugin/target/*.jar dist/plugins

# copy htmls
cp -r jeegot-web/src/main/resources/htmls dist

cp jeegot-web/start.bat dist
cp jeegot-web/start.sh dist

cd dist

# run main
mv jeegot-web-*-exec.jar jeegot-start.jar
