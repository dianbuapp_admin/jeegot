/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.core.wechat;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import com.jeegot.common.constants.JeegotConstants;
import com.jeegot.service.IConfigService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author： wjun_java@163.com
 * @date： 2021/6/6
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Component
public class DefaultWechatManager implements WechatManager, InitializingBean {

	@Autowired
	private WxMaService wxService;

	@Autowired
	private IConfigService configService;

	@Override
	public void init() throws Exception {
		WxMaDefaultConfigImpl config = new WxMaDefaultConfigImpl();
		String miniAppId = configService.getValue(JeegotConstants.WECHAT_MINI_APP_ID);
		String miniSecret = configService.getValue(JeegotConstants.WECHAT_MINI_APP_SECRET);
		if(StringUtils.isNotBlank(miniAppId) && StringUtils.isNotBlank(miniSecret)) {
			config.setAppid(miniAppId);
			config.setSecret(miniSecret);
			wxService.setWxMaConfig(config);
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		init();
	}
}
