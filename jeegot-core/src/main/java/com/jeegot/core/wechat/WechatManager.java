package com.jeegot.core.wechat;

/**
 * @author： wjun_java@163.com
 * @date： 2021/6/6
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
public interface WechatManager {

	void init() throws Exception;

}
