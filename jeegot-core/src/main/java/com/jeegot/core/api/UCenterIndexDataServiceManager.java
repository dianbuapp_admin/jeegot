/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.core.api;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author： wjun_java@163.com
 * @date： 2021/7/14
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Component
public class UCenterIndexDataServiceManager implements UCenterIndexDataService, InitializingBean, ApplicationContextAware {

	List<UCenterIndexDataService> indexDataServiceList = new ArrayList<>();

	ApplicationContext applicationContext;

	@Override
	public Map<String, Object> getIndexData(Long userId) {

		Map<String, Object> result = new HashMap<>();

		for (UCenterIndexDataService indexDataService : indexDataServiceList) {
			Map<String, Object> indexData = indexDataService.getIndexData(userId);
			result.putAll(indexData);
		}

		return result;
	}

	@Override
	public void addIndexDataService(UCenterIndexDataService indexDataService) {
		indexDataServiceList.add(indexDataService);
	}

	@Override
	public void removeIndexDataService(UCenterIndexDataService indexDataService) {
		indexDataServiceList.remove(indexDataService);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Map<String, UCenterIndexDataService> matchingBeans =
				BeanFactoryUtils.beansOfTypeIncludingAncestors(this.applicationContext, UCenterIndexDataService.class, true, false);
		matchingBeans.keySet().forEach(item -> {
			if(!item.equals("UCenterIndexDataServiceManager")) {
				addIndexDataService(matchingBeans.get(item));
			}
		});
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
