/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.core.plugin;

import com.jeegot.common.constants.JeegotConstants;
import org.pf4j.AbstractPluginManager;
import org.pf4j.RuntimeMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author： wjun_java@163.com
 * @date： 2021/4/27
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Configuration
public class PluginAutoConfiguration {

	@Autowired
	Environment environment;

	@Autowired
	private PluginConfig pluginConfig;

	@Bean
	public JeegotPluginManager pluginManager() {

		String profile = StringUtils.arrayToCommaDelimitedString(environment.getActiveProfiles());

		if(JeegotConstants.DEV_MODE.equals(profile)) {
			System.setProperty(AbstractPluginManager.MODE_PROPERTY_NAME, RuntimeMode.DEVELOPMENT.toString());
		}

		Path pluginPath = Paths.get(pluginConfig.getPath());
		JeegotPluginManager jeegotPluginManager = new JeegotPluginManager(pluginPath);
		return jeegotPluginManager;
	}

}
