/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.core.plugin.register;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author： wjun_java@163.com
 * @date： 2021/5/9
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Component
public class CompoundPluginRegister implements PluginRegister, ApplicationContextAware {

	ApplicationContext applicationContext;
	List<PluginRegister> pluginRegisterList = Collections.synchronizedList(new ArrayList<>());

	@Override
	public void initialize() throws Exception {
		pluginRegisterList.add(new ResourceLoaderPluginRegister());
		pluginRegisterList.add(new ClassPluginRegister());
//		pluginRegisterList.add(new PermissionPluginRegister(this.applicationContext));
		pluginRegisterList.add(new MapperPluginRegister());
		pluginRegisterList.add(new ApplicationContextPluginRegister());
		pluginRegisterList.add(new ControllerPluginRegister(this.applicationContext));
		pluginRegisterList.add(new FreeMarkerViewPluginRegister());
		pluginRegisterList.add(new DirectivePluginRegister());
		pluginRegisterList.add(new InterceptorPluginRegister(this.applicationContext));
		pluginRegisterList.add(new PaymentSuccessListenerRegister(this.applicationContext));
		pluginRegisterList.add(new ApiIndexDataServicePluginRegister(this.applicationContext));

		for (PluginRegister pluginRegister : pluginRegisterList) {
			pluginRegister.initialize();
		}

	}

	@Override
	public void registry(PluginRegistryWrapper pluginRegistryWrapper) throws Exception {

		for (PluginRegister pluginRegister : pluginRegisterList) {
			pluginRegister.registry(pluginRegistryWrapper);
		}

	}

	@Override
	public void unRegistry(PluginRegistryWrapper pluginRegistryWrapper) throws Exception {

		try {
			for (PluginRegister pluginRegister : pluginRegisterList) {
				pluginRegister.unRegistry(pluginRegistryWrapper);
			}
		} finally {
			pluginRegistryWrapper.getClassList().clear();
			PluginRegistryWrapperContextHolder.remove(pluginRegistryWrapper.getPluginWrapper().getPluginId());
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
