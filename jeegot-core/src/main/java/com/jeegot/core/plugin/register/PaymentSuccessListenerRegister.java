/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.core.plugin.register;

import com.jeegot.core.payment.listener.PaymentSuccessListener;
import com.jeegot.core.payment.listener.PaymentSuccessListenerManager;
import org.springframework.context.ApplicationContext;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author： wjun_java@163.com
 * @date： 2021/6/22
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
public class PaymentSuccessListenerRegister extends AbstractPluginRegister implements PluginRegister {

	PaymentSuccessListenerManager paymentSuccessListenerManager;

	ApplicationContext applicationContext;

	public PaymentSuccessListenerRegister(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Override
	public void initialize() throws Exception {
		paymentSuccessListenerManager = applicationContext.getBean(PaymentSuccessListenerManager.class);
	}

	@Override
	public void registry(PluginRegistryWrapper pluginRegistryWrapper) throws Exception {
		List<Class<?>> paymentSuccessListenerClassList = pluginRegistryWrapper.getClassList().stream().filter(clazz -> PaymentSuccessListener.class.isAssignableFrom(clazz)).collect(Collectors.toList());
		if(paymentSuccessListenerClassList != null && paymentSuccessListenerClassList.size() >0) {
			paymentSuccessListenerClassList.forEach(item -> paymentSuccessListenerManager.addListener((PaymentSuccessListener) pluginRegistryWrapper.getPluginApplicationContext().getBean(item)));
		}
	}

	@Override
	public void unRegistry(PluginRegistryWrapper pluginRegistryWrapper) throws Exception {
//		List<Class<?>> paymentSuccessListenerClassList = pluginRegistryWrapper.getClassList().stream().filter(clazz -> PaymentSuccessListener.class.isAssignableFrom(clazz)).collect(Collectors.toList());
//		if(paymentSuccessListenerClassList != null && paymentSuccessListenerClassList.size() >0) {
//			paymentSuccessListenerClassList.forEach(item -> paymentSuccessListenerManager.removeListener((PaymentSuccessListener) pluginRegistryWrapper.getPluginApplicationContext().getBean(item)));
//		}
	}

}
