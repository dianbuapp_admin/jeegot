/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.core.plugin;

import com.jeegot.common.constants.JeegotConstants;
import com.jeegot.common.exception.JeegotException;
import com.jeegot.core.plugin.register.PluginRegister;
import com.jeegot.core.plugin.register.PluginRegistryWrapper;
import com.jeegot.core.plugin.register.PluginRegistryWrapperContextHolder;
import com.jeegot.core.utils.StrUtils;
import lombok.extern.slf4j.Slf4j;
import org.pf4j.DefaultPluginManager;
import org.pf4j.PluginWrapper;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * @author： wjun_java@163.com
 * @date： 2021/4/21
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Slf4j
public class JeegotPluginManager extends DefaultPluginManager implements PluginManagerService, ApplicationContextAware, InitializingBean, EnvironmentAware {

    Environment environment;

    ApplicationContext applicationContext;

    @Autowired
    PluginRegister pluginRegister;

    public JeegotPluginManager(Path... pluginsRoots) {
        super(pluginsRoots);
    }

    @Override
    public void installPlugin(Path path) throws Exception {
        String pluginId = null;
        try {
            pluginId = loadPlugin(path);
            startPlugin(pluginId);
            PluginRegistryWrapperContextHolder.put(pluginId, new PluginRegistryWrapper(getPlugin(pluginId), applicationContext));
            pluginRegister.registry(PluginRegistryWrapperContextHolder.getPluginRegistryWrapper(pluginId));
        } catch (Exception e) {
            e.printStackTrace();
            if(StrUtils.isNotBlank(pluginId)) {
                PluginRegistryWrapperContextHolder.remove(pluginId);
            }
            throw new JeegotException(e.getMessage());
        }
    }

    @Override
    public void unInstallPlugin(String pluginId) throws Exception {

        PluginWrapper pluginWrapper = getPlugin(pluginId);
        if(!unloadPlugin(pluginId, false)) {
            throw new JeegotException("plugin [" + pluginId + "] 卸载失败");
        }

        log.info("unInstallPlugin:" + pluginId);
        if(!isDevMode()) {
            Files.delete(pluginWrapper.getPluginPath());
        }

        PluginRegistryWrapper pluginRegistryWrapper = PluginRegistryWrapperContextHolder.getPluginRegistryWrapper(pluginId);
        if(pluginRegistryWrapper != null) {
            try {
                pluginRegister.unRegistry(pluginRegistryWrapper);
            } catch (Exception e) {
                throw new JeegotException(e.getMessage());
            }
        }

    }

    @Override
    public void initPlugins() throws Exception {
        loadPlugins();
        startPlugins();
        pluginRegister.initialize();
        for (PluginWrapper startedPlugin : getPlugins()) {
            PluginRegistryWrapper pluginRegistryWrapper = new PluginRegistryWrapper(startedPlugin, applicationContext);
            PluginRegistryWrapperContextHolder.put(startedPlugin.getPluginId(), pluginRegistryWrapper);
            try {
                pluginRegister.registry(pluginRegistryWrapper);
            } catch (Exception e) {
                PluginRegistryWrapperContextHolder.remove(startedPlugin.getPluginId());
                unloadPlugin(pluginRegistryWrapper.getPluginWrapper().getPluginId());
                throw new JeegotException(e.getMessage());
            }
        }
    }

    @Override
    public List<PluginWrapper> getInstallPlugins() {
        return getPlugins();
    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    boolean isDevMode() {
        return JeegotConstants.DEV_MODE.equals(StringUtils.arrayToCommaDelimitedString(environment.getActiveProfiles()));
    }

    @Override
    public String loadPlugin(Path pluginPath) {
        if ((pluginPath == null) || Files.notExists(pluginPath)) {
            throw new IllegalArgumentException(String.format("Specified plugin %s does not exist!", pluginPath));
        }

        log.debug("Loading plugin from '{}'", pluginPath);

        PluginWrapper pluginWrapper = loadPluginFromPath(pluginPath);

        // try to resolve  the loaded plugin together with other possible plugins that depend on this plugin
        try {
            resolvePlugins();
        } catch (Exception e) {
            unloadPlugin(pluginWrapper.getPluginId());
        }

        return pluginWrapper.getDescriptor().getPluginId();
    }
}
