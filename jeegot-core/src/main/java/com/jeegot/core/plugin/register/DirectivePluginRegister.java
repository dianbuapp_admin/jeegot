/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.core.plugin.register;

import com.jeegot.core.directive.BaseDirective;
import com.jeegot.core.plugin.view.PluginFreeMarkerConfig;
import com.jeegot.core.template.JeegotTemplateFreeMarkerConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfig;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author： wjun_java@163.com
 * @date： 2021/5/9
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Slf4j
public class DirectivePluginRegister extends AbstractPluginRegister implements PluginRegister {

	@Override
	public void initialize() throws Exception {
	}

	@Override
	public void registry(PluginRegistryWrapper pluginRegistryWrapper) throws Exception {

		List<Class> directiveClassList = pluginRegistryWrapper.getClassList().stream().filter(item -> BaseDirective.class.isAssignableFrom(item)).collect(Collectors.toList());

		if(directiveClassList.isEmpty()) return;

		PluginFreeMarkerConfig pluginFreeMarkerConfig = pluginRegistryWrapper.getPluginApplicationContext().getBean(PluginFreeMarkerConfig.class);
		FreeMarkerConfig freeMarkerConfig = pluginRegistryWrapper.getPluginApplicationContext().getBean(FreeMarkerConfig.class);
		JeegotTemplateFreeMarkerConfig templateFreeMarkerConfig = pluginRegistryWrapper.getPluginApplicationContext().getBean(JeegotTemplateFreeMarkerConfig.class);

		Map<String, BaseDirective> matchingBeans = new HashMap<>();
		for (Class aClass : directiveClassList) {
			Map map = BeanFactoryUtils.beansOfTypeIncludingAncestors(pluginRegistryWrapper.getPluginApplicationContext(), aClass, true, false);
			matchingBeans.putAll(map);
		}

		matchingBeans.keySet().forEach(item -> {
			freeMarkerConfig.getConfiguration().setSharedVariable(item, matchingBeans.get(item));
			pluginFreeMarkerConfig.getConfiguration().setSharedVariable(item, matchingBeans.get(item));
			templateFreeMarkerConfig.getConfiguration().setSharedVariable(item, matchingBeans.get(item));
		});

	}

	@Override
	public void unRegistry(PluginRegistryWrapper pluginRegistryWrapper) throws Exception {

	}

}
