package com.jeegot.core.plugin;

import java.lang.annotation.*;

/**
 * @author： wjun_java@163.com
 * @date： 2021/5/20
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface UnLoad {
}
