package com.jeegot.core.plugin;

import org.pf4j.Plugin;

/**
 * @author： wjun_java@163.com
 * @date： 2021/8/3
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
public interface PluginListener {

	void start(Plugin plugin);

	void stop(Plugin plugin);

	void delete(Plugin plugin);

}
