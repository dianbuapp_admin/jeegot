/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.core.plugin;

import org.pf4j.Plugin;

import java.util.ArrayList;
import java.util.List;

/**
 * @author： wjun_java@163.com
 * @date： 2021/8/3
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
public class PluginListenerManager {

	private static final PluginListenerManager pluginListenerManager = new PluginListenerManager();

	List<PluginListener> pluginListenerList;

	public static PluginListenerManager me() {
		return pluginListenerManager;
	}

	public void addPluginListener(PluginListener pluginListener) {
		if(pluginListenerList == null) {
			pluginListenerList = new ArrayList<>();
		}
		pluginListenerList.add(pluginListener);
	}

	public void removePluginListener(PluginListener pluginListener) {
		if(pluginListenerList != null) {
			pluginListenerList.remove(pluginListener);
		}
	}

	public void notifyStart(Plugin plugin) {
		if(pluginListenerList != null) {
			pluginListenerList.forEach(item -> item.start(plugin));
		}
	}

	public void notifyStop(Plugin plugin) {
		if(pluginListenerList != null) {
			pluginListenerList.forEach(item -> item.stop(plugin));
		}
	}

	public void notifyDelete(Plugin plugin) {
		if(pluginListenerList != null) {
			pluginListenerList.forEach(item -> item.delete(plugin));
		}
	}

}
