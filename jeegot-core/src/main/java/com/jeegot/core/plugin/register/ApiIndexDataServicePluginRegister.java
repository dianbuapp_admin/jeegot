/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.core.plugin.register;

import com.jeegot.core.api.IndexDataService;
import com.jeegot.core.api.IndexDataServiceManager;
import com.jeegot.core.api.UCenterIndexDataService;
import com.jeegot.core.api.UCenterIndexDataServiceManager;
import org.springframework.context.ApplicationContext;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author： wjun_java@163.com
 * @date： 2021/7/4
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
public class ApiIndexDataServicePluginRegister extends AbstractPluginRegister implements PluginRegister {

	IndexDataServiceManager indexDataServiceManager;

	UCenterIndexDataServiceManager uCenterIndexDataServiceManager;

	ApplicationContext applicationContext;

	public ApiIndexDataServicePluginRegister(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Override
	public void initialize() throws Exception {
		indexDataServiceManager = applicationContext.getBean(IndexDataServiceManager.class);
		uCenterIndexDataServiceManager = applicationContext.getBean(UCenterIndexDataServiceManager.class);
	}

	@Override
	public void registry(PluginRegistryWrapper pluginRegistryWrapper) throws Exception {
		List<Class<?>> indexDataServiceClassList = pluginRegistryWrapper.getClassList().stream().filter(clazz -> IndexDataService.class.isAssignableFrom(clazz)).collect(Collectors.toList());
		if(indexDataServiceClassList != null && indexDataServiceClassList.size() >0) {
			indexDataServiceClassList.forEach(item -> indexDataServiceManager.addIndexDataService((IndexDataService) pluginRegistryWrapper.getPluginApplicationContext().getBean(item)));
		}

		List<Class<?>> indexUCenterDataServiceClassList = pluginRegistryWrapper.getClassList().stream().filter(clazz -> UCenterIndexDataService.class.isAssignableFrom(clazz)).collect(Collectors.toList());
		if(indexUCenterDataServiceClassList != null && indexUCenterDataServiceClassList.size() >0) {
			indexUCenterDataServiceClassList.forEach(item -> uCenterIndexDataServiceManager.addIndexDataService((UCenterIndexDataService) pluginRegistryWrapper.getPluginApplicationContext().getBean(item)));
		}

	}

	@Override
	public void unRegistry(PluginRegistryWrapper pluginRegistryWrapper) throws Exception {
//		List<Class<?>> indexDataServiceClassList = pluginRegistryWrapper.getClassList().stream().filter(clazz -> IndexDataService.class.isAssignableFrom(clazz)).collect(Collectors.toList());
//		if(indexDataServiceClassList != null && indexDataServiceClassList.size() >0) {
//			indexDataServiceClassList.forEach(item -> indexDataServiceManager.removeIndexDataService((IndexDataService) pluginRegistryWrapper.getPluginApplicationContext().getBean(item)));
//		}
//
//		List<Class<?>> indexUCenterDataServiceClassList = pluginRegistryWrapper.getClassList().stream().filter(clazz -> UCenterIndexDataService.class.isAssignableFrom(clazz)).collect(Collectors.toList());
//		if(indexUCenterDataServiceClassList != null && indexUCenterDataServiceClassList.size() >0) {
//			indexUCenterDataServiceClassList.forEach(item -> uCenterIndexDataServiceManager.removeIndexDataService((UCenterIndexDataService) pluginRegistryWrapper.getPluginApplicationContext().getBean(item)));
//		}

	}

}
