/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.core.config;

import com.jeegot.core.interceptor.AdminInterceptor;
import com.jeegot.core.interceptor.ApiInterceptor;
import com.jeegot.core.interceptor.UCenterInterceptor;
import com.jeegot.core.template.Template;
import com.jeegot.core.template.TemplateService;
import com.jeegot.core.utils.FileUtils;
import com.jeegot.core.utils.LocalDateTimeConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author： wjun_java@163.com
 * @date： 2021/2/17
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Slf4j
@Configuration
public class JeegotWebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private TemplateService templateService;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        final String uploadDir = FileUtils.getUploadDir();
        final String templateDir = FileUtils.getTemplateDir();

        List<String> locations = new ArrayList<>();
        locations.add("classpath:/static/");
        locations.add(ResourceUtils.FILE_URL_PREFIX + uploadDir);
        for (Template template : templateService.getTemplateList()) {
            locations.add(ResourceUtils.FILE_URL_PREFIX + templateDir + template.getPath() + "/static/");
        }

        registry.addResourceHandler("/static/**")
                .addResourceLocations(locations.toArray(new String[]{}));
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(adminInterceptor()).addPathPatterns("/admin/**");
        registry.addInterceptor(ucenterInterceptor()).addPathPatterns("/ucenter/**");
        registry.addInterceptor(apiInterceptor()).addPathPatterns("/api/**");
    }

    @Bean
    public AdminInterceptor adminInterceptor(){
        return new AdminInterceptor();
    }

    @Bean
    public UCenterInterceptor ucenterInterceptor(){
        return new UCenterInterceptor();
    }

    @Bean
    public ApiInterceptor apiInterceptor() {
        return new ApiInterceptor();
    }

    @Bean
    public SimpleMappingExceptionResolver simpleMappingExceptionResolver() {
        SimpleMappingExceptionResolver simpleMappingExceptionResolver = new SimpleMappingExceptionResolver();
        Properties mappings = new Properties();
        mappings.setProperty("org.springframework.web.multipart.MaxUploadSizeExceededException", "error");
        simpleMappingExceptionResolver.setExceptionMappings(mappings);
        return simpleMappingExceptionResolver;
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new LocalDateTimeConverter());
    }

}
