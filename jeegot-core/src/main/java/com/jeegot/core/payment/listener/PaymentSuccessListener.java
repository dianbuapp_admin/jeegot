package com.jeegot.core.payment.listener;

import com.jeegot.entity.PaymentRecord;

/**
 * @author： wjun_java@163.com
 * @date： 2021/6/22
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
public interface PaymentSuccessListener {

	void onSuccess(PaymentRecord paymentRecord);

}
