/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.core.xss;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author： wjun_java@163.com
 * @date： 2021/4/16
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
public class XssFilter implements Filter {

    String[] excludeUris = new String[] {"template/doEditSave", "article/doSave", "page/doSave", "product/doSave"};

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        if(needPassXss(req)) {
            chain.doFilter(req, resp);
        } else {
            chain.doFilter(new XssWrapper(req), resp);
        }
    }

    boolean needPassXss(HttpServletRequest request) {
        for (String uris : excludeUris) {
            if(request.getRequestURI().contains(uris)) {
                return true;
            }
        }
        return false;
    }

}
