package com.jeegot.mapper;

import com.jeegot.entity.PaymentRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付记录表 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-19
 */
public interface PaymentRecordMapper extends BaseMapper<PaymentRecord> {

}
