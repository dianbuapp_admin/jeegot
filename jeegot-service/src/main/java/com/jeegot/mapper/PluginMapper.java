package com.jeegot.mapper;

import com.jeegot.entity.Plugin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-08
 */
public interface PluginMapper extends BaseMapper<Plugin> {

}
