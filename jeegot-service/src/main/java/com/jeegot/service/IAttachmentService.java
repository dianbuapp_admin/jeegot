package com.jeegot.service;

import com.jeegot.entity.Attachment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  附件服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-02-19
 */
public interface IAttachmentService extends IService<Attachment> {

}
