package com.jeegot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeegot.entity.PaymentRecord;
import com.jeegot.mapper.PaymentRecordMapper;
import com.jeegot.service.IPaymentRecordService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付记录表 服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-19
 */
@Service
public class PaymentRecordServiceImpl extends ServiceImpl<PaymentRecordMapper, PaymentRecord> implements IPaymentRecordService {

	@Override
	public PaymentRecord getPaymentRecordByTrxNo(String trxNo) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("trx_no", trxNo);
		return getOne(queryWrapper);
	}
}
