package com.jeegot.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeegot.entity.UserOpenid;
import com.jeegot.mapper.UserOpenidMapper;
import com.jeegot.service.IUserOpenidService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账号绑定信息表 服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-08
 */
@Service
public class UserOpenidServiceImpl extends ServiceImpl<UserOpenidMapper, UserOpenid> implements IUserOpenidService {

	@Override
	public UserOpenid getByOpenid(String openid, String type) {
		LambdaQueryWrapper<UserOpenid> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(UserOpenid::getValue, openid);
		queryWrapper.eq(UserOpenid::getType, type);
		return getOne(queryWrapper);
	}

}
