package com.jeegot.mall.mapper;

import com.jeegot.mall.entity.ProductSpec;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品规格表 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface ProductSpecMapper extends BaseMapper<ProductSpec> {

}
