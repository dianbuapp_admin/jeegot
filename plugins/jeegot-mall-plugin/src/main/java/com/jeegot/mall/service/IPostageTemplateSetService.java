package com.jeegot.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jeegot.mall.entity.PostageTemplateSet;

import java.util.List;

/**
 * <p>
 * 运费设置 服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface IPostageTemplateSetService extends IService<PostageTemplateSet> {

	List<PostageTemplateSet> getTemplateSetList(Long templateId);

}
