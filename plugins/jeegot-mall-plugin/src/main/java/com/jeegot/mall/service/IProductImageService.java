package com.jeegot.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jeegot.mall.entity.ProductImage;

import java.util.List;

/**
 * <p>
 * 产品图片表 服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface IProductImageService extends IService<ProductImage> {

	List<ProductImage> getProductImageList(Long productId);

}
