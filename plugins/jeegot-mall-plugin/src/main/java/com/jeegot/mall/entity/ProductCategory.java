package com.jeegot.mall.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jeegot.common.constants.JeegotConfig;
import com.jeegot.mall.service.IProductService;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 商品分类表
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ProductCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String CATEGORY_TYPE = "category";
    public static final String TAG_TYPE = "tag";

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 父级分类的ID
     */
    private Long parentId;

    /**
     * 分类创建的用户ID
     */
    @TableField(fill = FieldFill.INSERT)
    private Long userId;

    /**
     * 分类名称
     */
    @NotBlank(message = "分类名称不能为空")
    private String title;

    /**
     * 类别 category tag ...
     */
    private String type = CATEGORY_TYPE;

    /**
     * 图标
     */
    private String icon;

    /**
     * 该分类的内容数量
     */
    private Integer count;

    /**
     * 排序编码
     */
    private Integer sortNum;

    /**
     * 描述
     */
    @TableField("`desc`")
    private String desc;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime created;

    /**
     * 修改日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updated;

    @TableField(exist = false)
    private String iconUrl;

    @TableField(exist = false)
    private List<IProductService.ProductVo> productList;

    public String getIconUrl() {
        return JeegotConfig.FILE_DOMAIN_URL + icon;
    }
}
