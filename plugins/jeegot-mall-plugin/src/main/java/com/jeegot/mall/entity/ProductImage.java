package com.jeegot.mall.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jeegot.common.constants.JeegotConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 产品图片表
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ProductImage implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long productId;

    private String imagePath;

    private Integer sortNum;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime created;

    @TableField(exist = false)
    private String imagePathUrl;

    public String getImagePathUrl() {
        return JeegotConfig.FILE_DOMAIN_URL + imagePath;
    }
}
