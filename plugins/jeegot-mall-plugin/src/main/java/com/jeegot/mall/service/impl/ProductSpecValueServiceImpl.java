package com.jeegot.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeegot.mall.entity.ProductSpecValue;
import com.jeegot.mall.mapper.ProductSpecValueMapper;
import com.jeegot.mall.service.IProductSpecValueService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 规格值 服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
@Service
public class ProductSpecValueServiceImpl extends ServiceImpl<ProductSpecValueMapper, ProductSpecValue> implements IProductSpecValueService {

	@Override
	public List<ProductSpecValue> getSpecValueList(Long specId) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("spec_id", specId);
		return list(queryWrapper);
	}

}
