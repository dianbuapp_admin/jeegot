/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.mall.api;

import com.jeegot.common.constants.JeegotConstants;
import com.jeegot.core.response.Response;
import com.jeegot.mall.service.IProductCategoryService;
import com.jeegot.web.controller.api.ApiBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author： wjun_java@163.com
 * @date： 2021/6/15
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Controller
@RequestMapping(JeegotConstants.API_MAPPING + "/category")
public class ProductCategoryApi extends ApiBaseController {

	@Autowired
	private IProductCategoryService productCategoryService;

	@GetMapping("list")
	public ResponseEntity list() {
		return Response.success(productCategoryService.getCategoryTreeList());
	}

	@GetMapping("get")
	public ResponseEntity get(Long categoryId) {
		return Response.success(productCategoryService.getCategoryInfo(categoryId));
	}

}
