package com.jeegot.mall.service.impl;

import com.jeegot.mall.entity.OrderInvoice;
import com.jeegot.mall.mapper.OrderInvoiceMapper;
import com.jeegot.mall.service.IOrderInvoiceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 发票信息表 服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-14
 */
@Service
public class OrderInvoiceServiceImpl extends ServiceImpl<OrderInvoiceMapper, OrderInvoice> implements IOrderInvoiceService {

}
