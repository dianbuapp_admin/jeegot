package com.jeegot.mall.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 运费模板表
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PostageTemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 邮费模板类型
     */
    public static final int TEMPLATE_TYPE_FREE = 1;         //包邮
    public static final int TEMPLATE_TYPE_CUSTOM = 2;       //自定义运费

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(fill = FieldFill.INSERT)
    private Long userId;

    /**
     * 运费模板名称
     */
    private String templateName;

    /**
     * 发货时间
     */
    private String shippingTime;

    /**
     * 计费方式 1按件，2按重量
     */
    private Integer valuationType;

    /**
     * 1包邮，2自定义运费
     */
    private Integer templateType;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime created;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updated;

    @TableField(exist = false)
    String[] templateSetId;
    @TableField(exist = false)
    String[] startStandards;
    @TableField(exist = false)
    String[] startFees;
    @TableField(exist = false)
    String[] addStandards;
    @TableField(exist = false)
    String[] addFees;
    @TableField(exist = false)
    String[] selectedareas;

}
