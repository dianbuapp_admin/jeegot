package com.jeegot.mall.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jeegot.mall.entity.ProductComment;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 * 商品评论表 服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface IProductCommentService extends IService<ProductComment> {

	/**
	 * 分页获取所有商品评论列表
	 * @param pageParam
	 * @param queryWrapper
	 * @return
	 */
	Page<ProductCommentVo> pageProductComment(Page pageParam, QueryWrapper queryWrapper);

	/**
	 * 分页查询具体某个商品的评论列表
	 * @param queryWrapper
	 * @return
	 */
	Page<ProductCommentVo> pageProductCommentByProductId(Page pageParam, QueryWrapper queryWrapper);

	/**
	 * 发布商品评论
	 * @param productComment
	 */
	void saveProductComment(ProductComment productComment) throws Exception;

	/**
	 * 获取商品一个优质评论
	 * @param productId
	 * @return
	 */
	ProductGoodCommentVo getProductGoodComment(Long productId);

	/**
	 * 统计各种状态下的评论数量
	 * @param productId
	 * @return
	 */
	ProductStatCommentVo getProductCommentStatCount(Long productId);

	@Data
	class ProductCommentVo extends ProductComment {
		String productTitle;
		String nickName;
		String headImg;
	}

	@Data
	class ProductGoodCommentVo extends ProductCommentVo {

		Long count;
		Long goodCount;
		float goodCommentRate;

		public float getGoodCommentRate() {
			if(count == null || count<=0 || goodCount == null || goodCount <=0) return new Float(100);
			BigDecimal bigDecimal = new BigDecimal(goodCount).divide(new BigDecimal(count), 2, BigDecimal.ROUND_HALF_UP);
			return bigDecimal.floatValue() * 100;
		}
	}

	@Data
	class ProductStatCommentVo extends ProductGoodCommentVo {
		Long middleCount;
		Long badCount;
		Long hasImageCount;
	}

}
