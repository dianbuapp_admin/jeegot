/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.mall.api;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jeegot.common.constants.JeegotConstants;
import com.jeegot.core.jwt.ApiToken;
import com.jeegot.core.response.Response;
import com.jeegot.mall.entity.ProductComment;
import com.jeegot.mall.service.IOrderItemService;
import com.jeegot.mall.service.IProductCommentService;
import com.jeegot.web.controller.api.ApiBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author： wjun_java@163.com
 * @date： 2021/6/25
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Controller
@RequestMapping(JeegotConstants.API_MAPPING + "/product/comment")
public class ProductCommentApi extends ApiBaseController {

	@Autowired
	private IProductCommentService productCommentService;

	@Autowired
	private IOrderItemService orderItemService;

	@GetMapping("stat/count")
	public ResponseEntity getProductCommentStatCount(@RequestParam(name = "productId") Long productId) {
		return Response.success(productCommentService.getProductCommentStatCount(productId));
	}

	@GetMapping("page")
	public ResponseEntity pageProductCommentByProductId(@RequestParam(name = "page", required = false, defaultValue = "1") Long page,
														@RequestParam(name = "pageSize", required = false, defaultValue = "10") Long pageSize,
														@RequestParam(name = "productId") Long productId,
														@RequestParam(name = "type") Integer type) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("product_id", productId);
		if(type == 1) {
			//最新
			queryWrapper.orderByDesc("created");
		} else if(type == 2) {
			//有图
			queryWrapper.eq("has_image", 1);
			queryWrapper.orderByDesc("created");
		} else if(type == 3) {
			//好评
			queryWrapper.ge("score", 3);
			queryWrapper.orderByDesc("created");
		} else if(type == 4) {
			//中评
			queryWrapper.eq("score", 3);
			queryWrapper.orderByDesc("created");
		} else if(type == 5) {
			//差评
			queryWrapper.le("score", 2);
			queryWrapper.orderByDesc("created");
		} else {
			queryWrapper.orderByDesc("score");
		}

		return Response.success(productCommentService.pageProductCommentByProductId(new Page(page, pageSize), queryWrapper));
	}

	@ApiToken
	@PostMapping("doSave")
	public ResponseEntity doSave(@RequestBody ProductComment productComment) {
		productComment.setUserId(getUserId());
		try {
			productCommentService.saveProductComment(productComment);
			return Response.success();
		} catch (Exception e) {
			return Response.fail(e.getMessage());
		}
	}

	@ApiToken
	@GetMapping("getListByOrder")
	public ResponseEntity getUserProductCommentListByOrder(Long orderId) {
		return Response.success(orderItemService.getOrderItemListOfComment(orderId, getUserId()));
	}

}
