package com.jeegot.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jeegot.mall.entity.ProductSkuItem;

import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface IProductSkuItemService extends IService<ProductSkuItem> {

	List<ProductSkuItem> getProductSkuItemList(Long productId);

	ProductSkuItem getProductSkuItem(Long productId, String skuName);

}
