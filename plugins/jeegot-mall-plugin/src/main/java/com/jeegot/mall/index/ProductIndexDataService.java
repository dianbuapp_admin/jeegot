/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.mall.index;

import com.jeegot.core.api.IndexDataService;
import com.jeegot.mall.entity.ProductCategory;
import com.jeegot.mall.service.IProductCategoryService;
import com.jeegot.mall.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author： wjun_java@163.com
 * @date： 2021/7/4
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Component
public class ProductIndexDataService implements IndexDataService {

	@Autowired
	private IProductService productService;

	@Autowired
	private IProductCategoryService productCategoryService;

	@Override
	public Map<String, Object> getIndexData(Long userId) {
		Map<String, Object> result= new HashMap<>();
		result.put("lunbo", productService.getProductListByTagName("轮播", 5));
		result.put("channel", getProductTagData("首页频道", 5));
		result.put("hot", getProductTagData("热门商品", 10));
		result.put("hotBig", getProductListByTag("热门大图", 3));
		result.put("goodTop", getProductTagData("官方精选", 10));
		result.put("goodTopBig", getProductListByTag("精选大图", 3));
		result.put("around", getProductTagData("品牌周边", 10));
		return result;
	}

	ProductCategory getProductTagData(String tag, Integer count) {
		ProductCategory productCategory = productCategoryService.getProductTag(tag);
		if(productCategory != null) {
			productCategory.setProductList(productService.getProductListByTagName(tag, count));
		}
		return productCategory;
	}

	List<IProductService.ProductVo> getProductListByTag(String tag, Integer count) {
		return productService.getProductListByTagName(tag, count);
	}

}
