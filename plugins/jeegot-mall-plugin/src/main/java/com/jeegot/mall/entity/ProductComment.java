package com.jeegot.mall.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeegot.common.constants.JeegotConfig;
import com.jeegot.core.utils.StrUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 商品评论表
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ProductComment implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String STATUS_NORMAL = "public"; //发布
    public static final String STATUS_UNAUDITED = "unaudited"; //待审核
    public static final String STATUS_HIDDEN = "hidden"; //隐藏

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 回复的评论ID
     */
    private Long parentId;

    /**
     * 评论的产品ID
     */
    private Long productId;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 评论的用户ID
     */
    @TableField(fill = FieldFill.INSERT)
    private Long userId;

    /**
     * 评论的内容
     */
    @NotBlank(message = "评论内容不能为空")
    private String content;

    /**
     * 评论的回复数量
     */
    private Integer replyCount;

    /**
     * 排序编号，常用语置顶等
     */
    private Integer sortNum;

    /**
     * “顶”的数量
     */
    private Integer upCount;

    /**
     * “踩”的数量
     */
    private Integer downCount;

    /**
     * 评论的状态
     */
    private String status;

    /**
     * 评论的图片，多张以","隔开
     */
    private String images;

    /**
     * 评分
     */
    private Integer score;

    /**
     * 商品sku
     */
    private String sku;

    /**
     * 是否有图
     */
    private Boolean hasImage;

    /**
     * 评论的时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime created;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updated;

    @TableField(exist = false)
    private String scoreTxt;

    @TableField(exist = false)
    private List<String> imageList;

    public String getScoreTxt() {
        if(score == null) return "";
        if(score == 1) {
            return "非常差";
        } else if(score == 2) {
            return "差";
        } else if(score == 3) {
            return "一般";
        } else if(score == 4) {
            return "满意";
        } else if(score == 5) {
            return "非常满意";
        }
        return "";
    }

    public List<String> getImageList() {
        List<String> imageList = new ArrayList<>();
        if(StrUtils.isNotBlank(this.images)) {
            String[] imageArr = this.images.split(",");
            for(String image : imageArr) {
                imageList.add(JeegotConfig.FILE_DOMAIN_URL + image);
            }
        }
        return imageList;
    }

}
