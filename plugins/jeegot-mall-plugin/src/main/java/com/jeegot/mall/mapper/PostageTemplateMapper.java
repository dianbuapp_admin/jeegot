package com.jeegot.mall.mapper;

import com.jeegot.mall.entity.PostageTemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 运费模板表 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface PostageTemplateMapper extends BaseMapper<PostageTemplate> {

}
