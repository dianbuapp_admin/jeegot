package com.jeegot.mall.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jeegot.common.constants.JeegotConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 订单明细表
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class OrderItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 订单号
     */
    private String orderSn;

    /**
     * 卖家id
     */
    private Long sellerId;

    /**
     * 产品id
     */
    private Long productId;

    /**
     * 产品数量
     */
    private Integer productCount;

    /**
     * 邮费
     */
    private BigDecimal postageCost;

    /**
     * 具体金额 = 产品价格+运费+其他价格
     */
    private BigDecimal totalAmount;

    /**
     * 商品规格
     */
    private String sku;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime updated;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime created;

    @TableField(exist = false)
    private String productTitle;

    @TableField(exist = false)
    private String productThumbnail;

    @TableField(exist = false)
    private BigDecimal price;

    @TableField(exist = false)
    private BigDecimal originPrice;

    @TableField(exist = false)
    private List<ProductComment> productCommentList;

    public String getProductThumbnail() {
        return JeegotConfig.FILE_DOMAIN_URL + productThumbnail;
    }
}
