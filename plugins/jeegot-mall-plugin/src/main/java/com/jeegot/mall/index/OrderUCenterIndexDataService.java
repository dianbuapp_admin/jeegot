/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.mall.index;

import com.jeegot.core.api.UCenterIndexDataService;
import com.jeegot.mall.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author： wjun_java@163.com
 * @date： 2021/7/14
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Component
public class OrderUCenterIndexDataService implements UCenterIndexDataService {

	@Autowired
	private IOrderService orderService;

	@Override
	public Map<String, Object> getIndexData(Long userId) {
		Map<String, Object> result = new HashMap<>();
		//统计订单个数
		IOrderService.OrderCountVo uCenterOrderCount = orderService.getUCenterOrderCount(userId);
		result.put("orderCount", uCenterOrderCount);
		return result;
	}

}
