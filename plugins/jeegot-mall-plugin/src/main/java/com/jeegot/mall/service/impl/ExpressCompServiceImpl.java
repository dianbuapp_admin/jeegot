package com.jeegot.mall.service.impl;

import com.jeegot.mall.entity.ExpressComp;
import com.jeegot.mall.mapper.ExpressCompMapper;
import com.jeegot.mall.service.IExpressCompService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-06
 */
@Service
public class ExpressCompServiceImpl extends ServiceImpl<ExpressCompMapper, ExpressComp> implements IExpressCompService {

}
