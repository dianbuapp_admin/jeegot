package com.jeegot.mall.mapper;

import com.jeegot.mall.entity.OrderDelivery;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-14
 */
public interface OrderDeliveryMapper extends BaseMapper<OrderDelivery> {

}
