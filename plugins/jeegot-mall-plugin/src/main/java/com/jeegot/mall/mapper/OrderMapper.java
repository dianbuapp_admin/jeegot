package com.jeegot.mall.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jeegot.mall.entity.Order;
import com.jeegot.mall.service.IOrderService;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-14
 */
public interface OrderMapper extends BaseMapper<Order> {

	Page<IOrderService.OrderVo> pageOrder(Page pageParam, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

	IOrderService.OrderCountVo getUCenterOrderCount(Long userId);

	IOrderService.OrderVo getOrderDetail(Long orderId);

	Page<Order> pageOrderOfApi(Page pageParam, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

}
