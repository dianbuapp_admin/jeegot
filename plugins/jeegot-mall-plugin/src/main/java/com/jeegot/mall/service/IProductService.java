package com.jeegot.mall.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jeegot.common.constants.JeegotConfig;
import com.jeegot.common.constants.JeegotConstants;
import com.jeegot.common.utils.JsoupUtils;
import com.jeegot.mall.entity.Product;
import com.jeegot.mall.entity.ProductCategory;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface IProductService extends IService<Product> {

	/**
	 * 保存商品信息
	 * @param product
	 * @throws Exception
	 */
	void saveProduct(Product product) throws Exception;

	/**
	 * 分页查询商品列表数据
	 * @param pageParam
	 * @param queryWrapper
	 * @return
	 */
	Page<ProductVo> pageProduct(Page pageParam, QueryWrapper queryWrapper);

	/**
	 * 获取商品详情
	 * @param id
	 * @return
	 */
	ProductInfoVo getProductById(Long id);

	/**
	 * 网站获取商品分页列表
	 * @param pageParam
	 * @param queryWrapper
	 * @return
	 */
	Page<ProductVo> pageProductByCategoryId(Page pageParam, QueryWrapper queryWrapper);

	/**
	 * 根据分类id查询商品列表
	 * @param queryWrapper
	 * @return
	 */
	List<ProductVo> getProductListByCategoryId(QueryWrapper queryWrapper);

	/**
	 * 根据标签获取商品列表
	 * @param tagName
	 * @param count
	 * @return
	 */
	List<ProductVo> getProductListByTagName(String tagName, Integer count);

	/**
	 * 更新商品浏览数量
	 * @param id
	 * @param count
	 */
	void updateViewCount(Long id, Long count);

	@Data
	class ProductVo implements Serializable {
		Long id;
		String title;
		String summary;
		BigDecimal price;
		BigDecimal originPrice;
		Integer stock;
		Integer salesCount;
		Integer viewCount;
		String status;
		LocalDateTime created;
		String author;
		String url;
		String thumbnail;
		List<ProductCategory> categoryList;

		public String getUrl() {
			return "/s/" + getId();
		}

		public String getThumbnail() {
			return JeegotConfig.FILE_DOMAIN_URL + thumbnail;
		}
	}

	@Data
	class ProductInfoVo extends Product {
		String author;
		String headImg;
		String headImgUrl;
		String contentHtmlView;
		List<ProductCategory> categoryList;

		List<ProductVo> recommendList;
		List<ProductVo> hotList;

		IProductCommentService.ProductGoodCommentVo goodComment;

		public String getHeadImgUrl() {
			return JeegotConstants.STATIC_RESOURCE_PATH + getHeadImg();
		}

		public String getContentHtmlView() {
			return JsoupUtils.parse(getContentHtml());
		}
	}

}
