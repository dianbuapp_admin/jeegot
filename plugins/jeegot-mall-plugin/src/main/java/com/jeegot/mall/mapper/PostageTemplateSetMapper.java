package com.jeegot.mall.mapper;

import com.jeegot.mall.entity.PostageTemplateSet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 运费设置 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface PostageTemplateSetMapper extends BaseMapper<PostageTemplateSet> {

}
