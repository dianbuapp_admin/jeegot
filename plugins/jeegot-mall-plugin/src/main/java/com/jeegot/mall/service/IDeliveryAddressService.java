package com.jeegot.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jeegot.mall.entity.DeliveryAddress;

import java.util.List;

/**
 * <p>
 * 收货地址 服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-18
 */
public interface IDeliveryAddressService extends IService<DeliveryAddress> {

	List<DeliveryAddress> getUserAddress(Long userId);

	DeliveryAddress saveAddress(DeliveryAddress deliveryAddress, Long userId) throws Exception;

	DeliveryAddress getDefault(Long userId);

}
