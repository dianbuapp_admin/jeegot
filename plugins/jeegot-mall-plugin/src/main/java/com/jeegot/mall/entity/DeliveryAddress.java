package com.jeegot.mall.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 收货地址
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DeliveryAddress implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long userId;

    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空")
    private String name;

    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空")
    private String mobile;

    /**
     * 详细地址
     */
    @NotBlank(message = "省市县地址不能为空")
    private String address;

    /**
     * 街道门牌
     */
    @NotBlank(message = "详细地址不能为空")
    private String street;

    /**
     * 是否默认,1是，0否
     */
    private Boolean isDefault;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updated;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime created;


}
