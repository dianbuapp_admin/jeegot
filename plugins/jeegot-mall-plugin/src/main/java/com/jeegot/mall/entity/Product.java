package com.jeegot.mall.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.jeegot.common.constants.JeegotConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String STATUS_PUBLISH = "publish";
    public static final String STATUS_DRAFT = "draft";
    public static final String STATUS_AUDIT = "audit";

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 商品的用户ID
     */
    @TableField(fill = FieldFill.INSERT)
    private Long userId;

    /**
     * 商品名称
     */
    @NotBlank(message = "商品名称不能为空")
    private String title;

    /**
     * 商品详情
     */
    @NotBlank(message = "商品详情不能为空")
    private String contentHtml;

    /**
     * 摘要
     */
    private String summary;

    /**
     * 缩略图
     */
    @NotBlank(message = "缩略图不能为空")
    private String thumbnail;

    /**
     * 视频
     */
    private String video;

    private String videoCover;

    /**
     * 排序编号
     */
    private Integer sortNum;

    /**
     * 商品价格
     */
    private BigDecimal price;

    /**
     * 原始价格
     */
    private BigDecimal originPrice;

    /**
     * 限时优惠价（早鸟价）
     */
    private BigDecimal limitedPrice;

    /**
     * 限时优惠截止时间
     */
    private LocalDateTime limitedTime;

    /**
     * 状态
     */
    private String status;

    /**
     * 运费模板
     */
    private Long postageTemplateId;

    /**
     * 评论状态，默认允许评论
     */
    private Boolean commentStatus;

    /**
     * 评论总数
     */
    private Integer commentCount;

    /**
     * 最后评论时间
     */
    private LocalDateTime commentTime;

    /**
     * 访问量
     */
    private Integer viewCount;

    /**
     * 真实访问量
     */
    private Integer realViewCount;

    /**
     * 销售量，用于放在前台显示
     */
    private Integer salesCount;

    /**
     * 真实的销售量
     */
    private Integer realSalesCount;

    /**
     * 剩余库存
     */
    private Integer stock;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime created;

    /**
     * 最后更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updated;

    @Version
    private Integer version;

    @TableField(exist = false)
    Long[] productCategory;

    @TableField(exist = false)
    String[] productTag;

    @TableField(exist = false)
    String url;

    @TableField(exist = false)
    private String thumbUrl;

    @TableField(exist = false)
    private String skuItems;

    @TableField(exist = false)
    String[] imageSrcs;

    @TableField(exist = false)
    List<ProductSkuItem> productSkuList;

    public String getUrl() {
        return "/s/" + getId();
    }

    public String getThumbUrl() {
        return JeegotConfig.FILE_DOMAIN_URL + getThumbnail();
    }

}
