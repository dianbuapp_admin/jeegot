package com.jeegot.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeegot.mall.entity.ProductCategory;
import com.jeegot.mall.mapper.ProductCategoryMapper;
import com.jeegot.mall.service.IProductCategoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 商品分类表 服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
@Service
public class ProductCategoryServiceImpl extends ServiceImpl<ProductCategoryMapper, ProductCategory> implements IProductCategoryService {

	@Override
	public ProductCategory getProductTag(String tag) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("title", tag);
		queryWrapper.eq("type", ProductCategory.TAG_TYPE);
		return getOne(queryWrapper);
	}

	@Override
	public List<ProductCategory> getCategoryList(Long userId) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("user_id", userId);
		queryWrapper.eq("type", ProductCategory.CATEGORY_TYPE);
		return list(queryWrapper);
	}

	@Override
	public List<ProductCategory> getTagList(Long userId) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("user_id", userId);
		queryWrapper.eq("type", ProductCategory.TAG_TYPE);
		return list(queryWrapper);
	}

	@Override
	public List<ProductCategoryVo> getCategoryTreeList() {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("type", ProductCategory.CATEGORY_TYPE);
		List<ProductCategory> productCategoryList = list(queryWrapper);
		List<ProductCategoryVo> productCategoryVoList = new ArrayList<>();
		productCategoryList.forEach(item -> {
			ProductCategoryVo productCategoryVo = new ProductCategoryVo();
			BeanUtils.copyProperties(item, productCategoryVo);
			productCategoryVoList.add(productCategoryVo);
		});

		List<ProductCategoryVo> resultVo = productCategoryVoList.stream().filter(item -> item.getParentId() == 0).sorted(Comparator.comparing(ProductCategoryVo::getSortNum)).collect(Collectors.toList());

		resultVo.forEach(item -> getChildren(item, productCategoryVoList));

		return resultVo;
	}

	@Override
	public ProductCategoryVo getCategoryInfo(Long categoryId) {
		ProductCategoryVo productCategoryVo = new ProductCategoryVo();
		ProductCategory category = getById(categoryId);
		if(category != null) {
			BeanUtils.copyProperties(category, productCategoryVo);
			List<ProductCategory> children = getChildren(category);
			if(children != null && children.size() >0) {
				List<ProductCategoryVo> productCategoryVoList = new ArrayList<>();
				children.forEach(item -> {
					ProductCategoryVo pv = new ProductCategoryVo();
					BeanUtils.copyProperties(item, pv);
					productCategoryVoList.add(pv);
				});
				productCategoryVo.setChildren(productCategoryVoList);
			}
		}
		return productCategoryVo;
	}

	private List<ProductCategory> getChildren(ProductCategory category) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("parent_id", category.getId());
		return list(queryWrapper);
	}

	private void getChildren(ProductCategoryVo productCategoryVo, List<ProductCategoryVo> productCategoryVoList) {
		List<ProductCategoryVo> children = productCategoryVoList.stream().filter(item -> Objects.equals(item.getParentId(), productCategoryVo.getId())).sorted(Comparator.comparing(ProductCategoryVo::getSortNum)).collect(Collectors.toList());
		if(children != null && children.size()>0) {
			productCategoryVo.setChildren(children);
			children.forEach(item -> getChildren(item, productCategoryVoList));
		}
	}

	@Override
	public void deleteByCategoryId(Long categoryId) {
		removeById(categoryId);
		getBaseMapper().deleteRelationByCategoryId(categoryId);
	}

	@Override
	public List<ProductCategory> getProductCategoryListByProductId(Long productId) {
		return getBaseMapper().getProductCategoryListByProductId(productId, ProductCategory.CATEGORY_TYPE);
	}

	@Override
	public List<ProductCategory> getProductTagListByProductId(Long productId) {
		return getBaseMapper().getProductCategoryListByProductId(productId, ProductCategory.TAG_TYPE);
	}

	@Override
	public Page<ProductCategoryVo> pageProductCategory(Page pageParam, QueryWrapper queryWrapper) {
		return getBaseMapper().pageProductCategory(pageParam, queryWrapper);
	}
}
