package com.jeegot.mall.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jeegot.mall.entity.ProductComment;
import com.jeegot.mall.service.IProductCommentService;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商品评论表 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface ProductCommentMapper extends BaseMapper<ProductComment> {

	Page<IProductCommentService.ProductCommentVo> pageProductComment(Page pageParam, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

	Page<IProductCommentService.ProductCommentVo> pageProductCommentByProductId(Page pageParam, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

	IProductCommentService.ProductGoodCommentVo getProductGoodComment(Long productId);

	IProductCommentService.ProductStatCommentVo getProductCommentStatCount(Long productId);

}
