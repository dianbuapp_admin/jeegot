package com.jeegot.mall.service;

import com.jeegot.mall.entity.ExpressComp;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-06
 */
public interface IExpressCompService extends IService<ExpressComp> {

}
