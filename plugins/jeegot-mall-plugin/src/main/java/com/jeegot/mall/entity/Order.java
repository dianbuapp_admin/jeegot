package com.jeegot.mall.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("`order`")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 交易状态
     */
    public static final int TRADE_STATUS_TRADING = 1;               //交易中
    public static final int TRADE_STATUS_COMPLETED = 2;             //交易完成（但是可以申请退款）
    public static final int TRADE_STATUS_CANCEL = 3;                //取消交易
    public static final int TRADE_STATUS_APPLY_FOR_REFUNDING = 4;   //申请退款
    public static final int TRADE_STATUS_REFUSAL_REFUNDING = 5;     //拒绝退款
    public static final int TRADE_STATUS_REFUNDING = 6;             //退款中
    public static final int TRADE_STATUS_REFUNDED = 7;              //退款完成
    public static final int TRADE_STATUS_FINISHED = 8;              //交易结束
    public static final int TRADE_STATUS_CLOSED = 9;                //订单关闭

    /**
     * payment 状态
     */
    public static final int STATUS_PAY_PRE = 0;     //未支付
    public static final int STATUS_PAY_SUCCESS = 1; //支付成功
    public static final int STATUS_PAY_FAILURE = 2; //支付失败

    /**
     * 发货状态（物流状态）
     */
    public static final int DELIVERY_STATUS_UNDELIVERY = 1;         //未发货
    public static final int DELIVERY_STATUS_DELIVERIED = 2;         //已发货
    public static final int DELIVERY_STATUS_NEED_RE_DELIVERY = 3;   //需要补发（特殊情况下，物流出现问题或者其他争议需要重新发货）
    public static final int DELIVERY_STATUS_FINISHED = 4;           //用户已收货
    public static final int DELIVERY_STATUS_NONEED = 5;             //无需发货

    /**
     * 发布开具状态
     */
    public static final int INVOICE_STATUS_NOT_APPLY = 1;           //未申请发票
    public static final int INVOICE_STATUS_APPLYING = 2;            //发票申请中
    public static final int INVOICE_STATUS_INVOICING = 3;           //发票开具中
    public static final int INVOICE_STATUS_WITHOUT = 4;              //无需开具发票
    public static final int INVOICE_STATUS_INVOICED = 5;            //发票已经开具

    /**
     * 订单删除状态
     */
    public static final int ORDER_STATUS_NORMAL = 1;           //正常状态
    public static final int ORDER_STATUS_DEL = 0;              //已经删除

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 订单号
     */
    private String orderSn;

    /**
     * 购买人
     */
    private Long userId;

    /**
     * 订单标题
     */
    private String orderTitle;

    /**
     * 用户留言
     */
    private String buyerMsg;

    /**
     * 订单总金额，购买人员应该付款的金额
     */
    private BigDecimal orderAmount;

    /**
     * 支付方式
     */
    private String payType;

    /**
     * 支付状态
     */
    private Integer payStatus;

    /**
     * 支付记录
     */
    private Long paymentId;

    /**
     * 发货情况
     */
    private Long deliveryId;

    /**
     * 1待发货，2已发货
     */
    private Integer deliveryStatus;

    /**
     * 收货人地址
     */
    private String consigneeUsername;

    /**
     * 收货人手机号（电话）
     */
    private String consigneeMobile;

    /**
     * 收件人的详细地址
     */
    private String consigneeAddrDetail;

    /**
     * 发票
     */
    private Integer invoiceId;

    /**
     * 发票开具状态：1 未申请发票、 2 发票申请中、 3 发票开具中、 4 无需开具发票、 5发票已经开具
     */
    private Integer invoiceStatus;

    /**
     * 订单邮费
     */
    private BigDecimal postageAmount;

    /**
     * 订单最终支付金额
     */
    private BigDecimal payAmount;

    /**
     * 管理员后台备注
     */
    private String remarks;

    /**
     * 交易状态：1交易中、 2交易完成（但是可以申请退款） 、3取消交易 、4申请退款、 5拒绝退款、 6退款中、 7退款完成、 8交易结束
     */
    private Integer tradeStatus;

    /**
     * 优惠码
     */
    private String couponCode;

    /**
     * 优惠金额
     */
    private BigDecimal couponAmount;

    /**
     * 删除状态：1 正常 ，2 已经删除
     */
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime created;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updated;

    @Version
    private Integer version;

    @TableField(exist = false)
    List<OrderItem> orderItemList;

}
