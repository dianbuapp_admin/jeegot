package com.jeegot.mall.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jeegot.aspect.DataPermission;
import com.jeegot.mall.entity.Product;
import com.jeegot.mall.entity.ProductCategory;
import com.jeegot.mall.service.IProductService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface ProductMapper extends BaseMapper<Product> {

	void deleteRelationByProductId(Long productId);

	void saveProductCategoryRelation(Long productId, List<Long> productCategoryIdList);

	@DataPermission("a")
	Page<IProductService.ProductVo> pageProduct(Page pageParam, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

	List<ProductCategory> getProductCategoriesByProductId(Long productId);

	IProductService.ProductInfoVo getProductById(Object id);

	Page<IProductService.ProductVo> pageProductByCategoryId(Page pageParam, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

	List<IProductService.ProductVo> getProductListByCategoryId(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);

	List<IProductService.ProductVo> getProductListByTagName(String tagName, Integer count);

	void updateViewCount(@Param("id") Long id, @Param("count") Long count);

}
