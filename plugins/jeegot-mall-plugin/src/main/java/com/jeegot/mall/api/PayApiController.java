/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.mall.api;

import com.egzosn.pay.wx.bean.WxTransactionType;
import com.jeegot.common.constants.JeegotConstants;
import com.jeegot.core.jwt.ApiToken;
import com.jeegot.core.payment.PayServiceManager;
import com.jeegot.core.payment.config.JeegotPayOrder;
import com.jeegot.core.payment.platform.WxPaymentPlatform;
import com.jeegot.core.response.Response;
import com.jeegot.core.utils.RequestUtils;
import com.jeegot.entity.PaymentRecord;
import com.jeegot.mall.entity.Order;
import com.jeegot.mall.service.IOrderService;
import com.jeegot.web.controller.api.ApiBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @author： wjun_java@163.com
 * @date： 2021/6/18
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Controller
@RequestMapping(JeegotConstants.API_MAPPING + "/pay")
public class PayApiController extends ApiBaseController {

	@Autowired
	private IOrderService orderService;

	@Autowired
	private PayServiceManager payServiceManager;

	@GetMapping("wechat/mini")
	@ApiToken
	public ResponseEntity wechatMini(Long orderId) {

		Order order = orderService.getById(orderId);
		if(order == null) {
			return Response.fail("订单不存在");
		}

		if(order.getPayStatus() == Order.STATUS_PAY_SUCCESS) {
			return Response.fail("订单已支付");
		}

		order.setPayType(WxPaymentPlatform.platformName);
		PaymentRecord paymentRecord = orderService.preparePaymentRecord(order, getOpenid(), RequestUtils.getIpAddress(RequestUtils.getRequest()));

		JeegotPayOrder jeegotPayOrder = new JeegotPayOrder(WxPaymentPlatform.platformName, WxTransactionType.JSAPI.name(), order.getOrderTitle(), order.getBuyerMsg(), order.getPayAmount(), paymentRecord.getTrxNo());
		jeegotPayOrder.setOpenid(getOpenid());

		try {
			Map<String, Object> orderInfo = payServiceManager.getOrderInfo(jeegotPayOrder);
			return Response.success(orderInfo);
		} catch (Exception e) {
			return Response.fail(e.getMessage());
		}

	}

}
