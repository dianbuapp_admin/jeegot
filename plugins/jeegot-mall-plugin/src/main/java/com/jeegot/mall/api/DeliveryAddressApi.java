/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.mall.api;

import com.jeegot.common.constants.JeegotConstants;
import com.jeegot.core.jwt.ApiToken;
import com.jeegot.core.response.Response;
import com.jeegot.mall.entity.DeliveryAddress;
import com.jeegot.mall.service.IDeliveryAddressService;
import com.jeegot.web.controller.api.ApiBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Objects;

/**
 * @author： wjun_java@163.com
 * @date： 2021/6/18
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Controller
@RequestMapping(JeegotConstants.API_MAPPING + "/address")
public class DeliveryAddressApi extends ApiBaseController {

	@Autowired
	private IDeliveryAddressService deliveryAddressService;

	@ApiToken
	@GetMapping("list")
	public ResponseEntity list() {
		return Response.success(deliveryAddressService.getUserAddress(getUserId()));
	}

	@ApiToken
	@GetMapping("getDefault")
	public ResponseEntity getDefault() {
		return Response.success(deliveryAddressService.getDefault(getUserId()));
	}

	@ApiToken
	@GetMapping("get")
	public ResponseEntity get(Long addressId) {
		return Response.success(deliveryAddressService.getById(addressId));
	}

	@ApiToken
	@PostMapping("doSave")
	public ResponseEntity doSave(@Validated @RequestBody DeliveryAddress deliveryAddress) {
		try {
			return Response.success(deliveryAddressService.saveAddress(deliveryAddress, getUserId()));
		} catch (Exception e) {
			return Response.fail(e.getMessage());
		}
	}

	@ApiToken
	@PostMapping("doDelete")
	public ResponseEntity doDelete(Long addressId) {
		DeliveryAddress address = deliveryAddressService.getById(addressId);
		if(address == null) {
			return Response.fail("地址不存在");
		}

		if(!Objects.equals(address.getUserId(), getUserId())) {
			return Response.fail("删除失败");
		}

		return Response.success(deliveryAddressService.removeById(addressId));
	}

}
