package com.jeegot.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeegot.mall.entity.PostageTemplateSet;
import com.jeegot.mall.mapper.PostageTemplateSetMapper;
import com.jeegot.mall.service.IPostageTemplateSetService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 运费设置 服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
@Service
public class PostageTemplateSetServiceImpl extends ServiceImpl<PostageTemplateSetMapper, PostageTemplateSet> implements IPostageTemplateSetService {

	@Override
	public List<PostageTemplateSet> getTemplateSetList(Long templateId) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("template_id", templateId);
		return list(queryWrapper);
	}

}
