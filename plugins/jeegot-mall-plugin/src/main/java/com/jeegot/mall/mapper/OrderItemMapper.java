package com.jeegot.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jeegot.mall.entity.OrderItem;

import java.util.List;

/**
 * <p>
 * 订单明细表 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-14
 */
public interface OrderItemMapper extends BaseMapper<OrderItem> {

	List<OrderItem> getOrderItemListOfComment(Long orderId);

}
