package com.jeegot.mall.mapper;

import com.jeegot.mall.entity.ProductSkuItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface ProductSkuItemMapper extends BaseMapper<ProductSkuItem> {

}
