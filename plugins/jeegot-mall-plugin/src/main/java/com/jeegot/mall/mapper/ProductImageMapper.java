package com.jeegot.mall.mapper;

import com.jeegot.mall.entity.ProductImage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 产品图片表 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface ProductImageMapper extends BaseMapper<ProductImage> {

}
