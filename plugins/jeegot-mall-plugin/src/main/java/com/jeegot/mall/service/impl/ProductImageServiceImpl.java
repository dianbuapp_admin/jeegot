package com.jeegot.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeegot.mall.entity.ProductImage;
import com.jeegot.mall.mapper.ProductImageMapper;
import com.jeegot.mall.service.IProductImageService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 产品图片表 服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
@Service
public class ProductImageServiceImpl extends ServiceImpl<ProductImageMapper, ProductImage> implements IProductImageService {

	@Override
	public List<ProductImage> getProductImageList(Long productId) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("product_id", productId);
		return list(queryWrapper);
	}
}
