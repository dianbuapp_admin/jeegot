package com.jeegot.mall.mapper;

import com.jeegot.mall.entity.DeliveryAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 收货地址 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-18
 */
public interface DeliveryAddressMapper extends BaseMapper<DeliveryAddress> {

}
