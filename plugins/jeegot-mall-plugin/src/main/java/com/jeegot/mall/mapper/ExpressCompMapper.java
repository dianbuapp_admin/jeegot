package com.jeegot.mall.mapper;

import com.jeegot.mall.entity.ExpressComp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-06
 */
public interface ExpressCompMapper extends BaseMapper<ExpressComp> {

}
