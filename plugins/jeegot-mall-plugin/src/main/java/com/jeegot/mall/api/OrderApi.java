/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.mall.api;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jeegot.common.constants.JeegotConstants;
import com.jeegot.core.jwt.ApiToken;
import com.jeegot.core.response.Response;
import com.jeegot.mall.entity.Order;
import com.jeegot.mall.service.IOrderService;
import com.jeegot.mall.service.IPostageTemplateService;
import com.jeegot.web.controller.api.ApiBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @author： wjun_java@163.com
 * @date： 2021/6/18
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Controller
@RequestMapping(JeegotConstants.API_MAPPING + "/order")
public class OrderApi extends ApiBaseController {

	@Autowired
	private IOrderService orderService;

	@Autowired
	private IPostageTemplateService postageTemplateService;

	@ApiToken
	@PostMapping("doCreateOrder")
	public ResponseEntity doCreateOrder(@Validated @RequestBody IOrderService.CreateOrderParam createOrderParam) {
		try {
			createOrderParam.setUserId(getUserId());
			return Response.success(orderService.createOrder(createOrderParam));
		} catch (Exception e) {
			e.printStackTrace();
			return Response.fail(e.getMessage());
		}
	}

	@ApiToken
	@PostMapping("getPostageFee")
	public ResponseEntity getPostageFee(@RequestBody IPostageTemplateService.GetPostageAmountParam getPostageAmountParam) {
		return Response.success(postageTemplateService.getPostageAmount(getPostageAmountParam));
	}

	@ApiToken
	@GetMapping("page")
	public ResponseEntity page(
			@RequestParam(name = "page", required = false, defaultValue = "1") Long page,
			@RequestParam(name = "pageSize", required = false, defaultValue = "10") Long pageSize,
			@RequestParam(name = "status", required = false) Long status
	) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("o.user_id", getUserId());

		if(status == 1) {
			//待付款订单
			queryWrapper.eq("pay_status", Order.STATUS_PAY_PRE);
		} else if(status == 2) {
			//待发货订单
			queryWrapper.eq("delivery_status", Order.DELIVERY_STATUS_UNDELIVERY);
		} else if(status == 3) {
			//已发货订单
			queryWrapper.eq("delivery_status", Order.DELIVERY_STATUS_DELIVERIED);
		} else if(status == 4) {
			//待评价订单
			queryWrapper.eq("delivery_status", Order.DELIVERY_STATUS_FINISHED);
			queryWrapper.eq("trade_status", Order.TRADE_STATUS_TRADING);
		}

		//已取消的订单不查
		queryWrapper.ne("trade_status", Order.TRADE_STATUS_CANCEL);

		return Response.success(orderService.pageOrderOfApi(new Page(page, pageSize), queryWrapper));
	}

	@ApiToken
	@GetMapping("detail")
	public ResponseEntity detail(Long orderId) {
		return Response.success(orderService.getOrderDetail(orderId));
	}

	@ApiToken
	@GetMapping("doCancel")
	public ResponseEntity cancel(Long orderId) {
		Order order = orderService.getById(orderId);
		if(order == null) {
			return Response.fail("订单不存在");
		}
		if(!Objects.equals(order.getUserId(), getUserId())) {
			return Response.fail("不能取消别人的订单");
		}
		if(order.getPayStatus() != Order.STATUS_PAY_PRE) {
			return Response.fail("订单当前状态不可取消");
		}

		order.setTradeStatus(Order.TRADE_STATUS_CANCEL);
		orderService.updateById(order);

		return Response.success();
	}

	@ApiToken
	@GetMapping("doConfirmDelivery")
	public ResponseEntity confirmDelivery(Long orderId) {
		Order order = orderService.getById(orderId);
		if(order == null) {
			return Response.fail("订单不存在");
		}
		if(!Objects.equals(order.getUserId(), getUserId())) {
			return Response.fail("不是自己的订单，无权确认收货");
		}
		if(order.getPayStatus() == Order.STATUS_PAY_PRE) {
			return Response.fail("当前订单未支付");
		}
		if(order.getDeliveryStatus() == Order.DELIVERY_STATUS_UNDELIVERY) {
			return Response.fail("当前订单未发货");
		}

		order.setDeliveryStatus(Order.DELIVERY_STATUS_FINISHED);
		orderService.updateById(order);

		return Response.success();
	}

}
