package com.jeegot.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeegot.core.utils.StrUtils;
import com.jeegot.mall.entity.Order;
import com.jeegot.mall.entity.ProductComment;
import com.jeegot.mall.mapper.ProductCommentMapper;
import com.jeegot.mall.service.IOrderService;
import com.jeegot.mall.service.IProductCommentService;
import com.jeegot.mall.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 商品评论表 服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
@Service
public class ProductCommentServiceImpl extends ServiceImpl<ProductCommentMapper, ProductComment> implements IProductCommentService {

	@Autowired
	IProductService productService;

	@Autowired
	IOrderService orderService;

	@Override
	public Page<ProductCommentVo> pageProductComment(Page pageParam, QueryWrapper queryWrapper) {
		return getBaseMapper().pageProductComment(pageParam, queryWrapper);
	}

	@Override
	public Page<ProductCommentVo> pageProductCommentByProductId(Page pageParam, QueryWrapper queryWrapper) {
		return getBaseMapper().pageProductCommentByProductId(pageParam, queryWrapper);
	}

	@Override
	@Transactional
	public void saveProductComment(ProductComment productComment) throws Exception {
		productComment.setStatus(ProductComment.STATUS_NORMAL);

		if(StrUtils.isNotBlank(productComment.getImages())) {
			productComment.setHasImage(true);
		}

		saveOrUpdate(productComment);
		Order order = orderService.getById(productComment.getOrderId());
		order.setTradeStatus(Order.TRADE_STATUS_COMPLETED);
		orderService.updateById(order);
	}

	@Override
	public ProductGoodCommentVo getProductGoodComment(Long productId) {
		return getBaseMapper().getProductGoodComment(productId);
	}

	@Override
	public ProductStatCommentVo getProductCommentStatCount(Long productId) {
		return getBaseMapper().getProductCommentStatCount(productId);
	}
}
