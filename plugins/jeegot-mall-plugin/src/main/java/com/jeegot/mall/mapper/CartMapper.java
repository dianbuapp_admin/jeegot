package com.jeegot.mall.mapper;

import com.jeegot.mall.entity.Cart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-14
 */
public interface CartMapper extends BaseMapper<Cart> {

}
