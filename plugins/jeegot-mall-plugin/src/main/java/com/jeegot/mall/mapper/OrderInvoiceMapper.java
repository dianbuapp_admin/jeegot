package com.jeegot.mall.mapper;

import com.jeegot.mall.entity.OrderInvoice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 发票信息表 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-14
 */
public interface OrderInvoiceMapper extends BaseMapper<OrderInvoice> {

}
