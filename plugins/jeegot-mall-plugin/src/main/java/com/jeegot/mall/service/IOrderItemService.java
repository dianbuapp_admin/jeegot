package com.jeegot.mall.service;

import com.jeegot.mall.entity.OrderItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 订单明细表 服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-14
 */
public interface IOrderItemService extends IService<OrderItem> {

	List<OrderItem> getOrderItemListOfComment(Long orderId, Long userId);

}
