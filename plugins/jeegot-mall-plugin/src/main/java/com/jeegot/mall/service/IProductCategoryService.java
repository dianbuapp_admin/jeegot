package com.jeegot.mall.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jeegot.mall.entity.Product;
import com.jeegot.mall.entity.ProductCategory;
import lombok.Data;

import java.util.List;

/**
 * <p>
 * 商品分类表 服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface IProductCategoryService extends IService<ProductCategory> {

	/**
	 * 根据标签名称获取商品标签
	 * @param tag
	 * @return
	 */
	ProductCategory getProductTag(String tag);

	/**
	 * 获取用户分类集合
	 * @param userId
	 * @return
	 */
	List<ProductCategory> getCategoryList(Long userId);

	/**
	 * 获取用户标签集合
	 * @return
	 */
	List<ProductCategory> getTagList(Long userId);

	/**
	 * 获取所有分类含子分类的数据
	 * @return
	 */
	List<ProductCategoryVo> getCategoryTreeList();

	/**
	 * 根据id获取分类
	 * @param categoryId
	 * @return
	 */
	ProductCategoryVo getCategoryInfo(Long categoryId);

	/**
	 * 删除分类
	 * @param categoryId
	 */
	void deleteByCategoryId(Long categoryId);

	/**
	 * 获取商品已设置的分类
	 * @param productId
	 * @return
	 */
	List<ProductCategory> getProductCategoryListByProductId(Long productId);

	/**
	 * 获取商品已设置的标签
	 * @param productId
	 * @return
	 */
	List<ProductCategory> getProductTagListByProductId(Long productId);

	/**
	 * 分页查询商品分类列表
	 * @param pageParam
	 * @param queryWrapper
	 * @return
	 */
	Page<ProductCategoryVo> pageProductCategory(Page pageParam, QueryWrapper queryWrapper);

	@Data
	class ProductCategoryVo extends ProductCategory {
		String createdUser;
		private List<ProductCategoryVo> children;
	}

}
