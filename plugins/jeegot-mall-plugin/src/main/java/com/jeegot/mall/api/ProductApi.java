/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.mall.api;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jeegot.common.constants.JeegotConstants;
import com.jeegot.core.response.Response;
import com.jeegot.mall.entity.Product;
import com.jeegot.mall.service.IProductService;
import com.jeegot.web.controller.api.ApiBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author： wjun_java@163.com
 * @date： 2021/6/13
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Controller
@RequestMapping(JeegotConstants.API_MAPPING + "/product")
public class ProductApi extends ApiBaseController {

	@Autowired
	private IProductService productService;

	@GetMapping("page")
	public ResponseEntity page(@RequestParam(name = "page", required = false, defaultValue = "1") Long page,
							   @RequestParam(name = "pageSize", required = false, defaultValue = "10") Long pageSize,
							   @RequestParam(name = "categoryId", required = false) Long categoryId) {

		QueryWrapper queryWrapper = new QueryWrapper();
		if(categoryId != null) {
			queryWrapper.eq("acr.category_id", categoryId);
		}
		queryWrapper.eq("a.status", Product.STATUS_PUBLISH);

		Page<IProductService.ProductVo> productVoPage = productService.pageProductByCategoryId(new Page(page, pageSize), queryWrapper);

		return Response.success(productVoPage);
	}

	@GetMapping("detail")
	public ResponseEntity detail(Long id) {
		IProductService.ProductInfoVo product = productService.getProductById(id);
		return Response.success(product);
	}

	@GetMapping("listByTag")
	public ResponseEntity listByTag(@RequestParam(name = "tag") String tag,
									@RequestParam(name = "count", defaultValue = "5") Integer count) {
		return Response.success(productService.getProductListByTagName(tag, count));
	}

}
