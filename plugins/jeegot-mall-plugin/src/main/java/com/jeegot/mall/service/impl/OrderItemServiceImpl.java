package com.jeegot.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeegot.mall.entity.OrderItem;
import com.jeegot.mall.entity.ProductComment;
import com.jeegot.mall.mapper.OrderItemMapper;
import com.jeegot.mall.service.IOrderItemService;
import com.jeegot.mall.service.IProductCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 订单明细表 服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-14
 */
@Service
public class OrderItemServiceImpl extends ServiceImpl<OrderItemMapper, OrderItem> implements IOrderItemService {

	@Autowired
	private IProductCommentService productCommentService;

	@Override
	public List<OrderItem> getOrderItemListOfComment(Long orderId, Long userId) {
		List<OrderItem> orderItemListOfComment = getBaseMapper().getOrderItemListOfComment(orderId);
		for(OrderItem orderItem : orderItemListOfComment) {
			QueryWrapper queryWrapper = new QueryWrapper();
			queryWrapper.eq("product_id", orderItem.getProductId());
			queryWrapper.eq("user_id", userId);
			queryWrapper.eq("order_id", orderItem.getOrderId());
			List<ProductComment> productCommentList = productCommentService.list(queryWrapper);
			orderItem.setProductCommentList(productCommentList);
		}
		return orderItemListOfComment;
	}

}
