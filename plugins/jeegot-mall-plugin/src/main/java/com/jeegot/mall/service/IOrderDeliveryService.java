package com.jeegot.mall.service;

import com.jeegot.mall.entity.OrderDelivery;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-14
 */
public interface IOrderDeliveryService extends IService<OrderDelivery> {

}
