package com.jeegot.mall.mapper;

import com.jeegot.mall.entity.ProductSpecValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 规格值 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface ProductSpecValueMapper extends BaseMapper<ProductSpecValue> {

}
