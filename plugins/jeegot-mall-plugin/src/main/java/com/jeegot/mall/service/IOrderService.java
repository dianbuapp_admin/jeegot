package com.jeegot.mall.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jeegot.entity.PaymentRecord;
import com.jeegot.mall.entity.Order;
import com.jeegot.mall.entity.OrderDelivery;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-14
 */
public interface IOrderService extends IService<Order> {

	/**
	 * 管理后台分页获取订单列表
	 * @param pageParam
	 * @param queryWrapper
	 * @return
	 */
	Page<OrderVo> pageOrder(Page pageParam, QueryWrapper queryWrapper);

	Long createOrder(CreateOrderParam createOrderParam) throws Exception;

	PaymentRecord preparePaymentRecord(Order order, String openid, String ip);

	OrderCountVo getUCenterOrderCount(Long userId);

	Page<Order> pageOrderOfApi(Page pageParam, QueryWrapper queryWrapper);

	OrderVo getOrderDetail(Long orderId);

	OrderDelivery getOrderDelivery(Long orderId);

	void doOrderDelivery(Long orderId, String expName, String billNumber, String deliveryTime) throws Exception;

	@Data
	class OrderVo extends Order {
		String nickName;
	}

	@Data
	class OrderCountVo implements Serializable {
		Integer allCount;
		Integer unPay;		//未支付
		Integer toSend;		//已支付，待发货
		Integer send;		//卖家已发货
		Integer comment;	//买家已确认收货(待评价)
		Integer success;	//已评价，交易成功
		Integer close;		//订单关闭 订单超时未付款被关闭
		Integer afterSale;	//售后
	}

	@Data
	class CreateOrderParam implements Serializable {
		Long userId;
		List<ProductParam> products;
		Long addressId;
		String buyerMsg;
		/**
		 * 优惠券id
		 */
		Long couponId;
	}

	@Data
	class ProductParam implements Serializable {
		Long id;
		Long num;
		String sku;
	}
}
