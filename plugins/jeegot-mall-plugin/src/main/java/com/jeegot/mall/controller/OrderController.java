/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.mall.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jeegot.aspect.DataPermission;
import com.jeegot.common.constants.JeegotConstants;
import com.jeegot.core.permission.AdminMenu;
import com.jeegot.core.response.Response;
import com.jeegot.core.utils.StrUtils;
import com.jeegot.mall.entity.Order;
import com.jeegot.mall.service.IExpressCompService;
import com.jeegot.mall.service.IOrderService;
import com.jeegot.web.controller.admin.AdminBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * @author： wjun_java@163.com
 * @date： 2021/6/14
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Controller
@RequestMapping(JeegotConstants.ADMIN_MAPPING + "/order")
@AdminMenu(name = "订单", icon = "<i class=\"nav-icon fas fa-shopping-cart\"></i>", sort = 2)
public class OrderController extends AdminBaseController {

	@Autowired
	private IOrderService orderService;

	@Autowired
	private IExpressCompService expressCompService;

	@DataPermission("o")
	@GetMapping("list")
	@AdminMenu(name = "订单管理", sort = 1)
	public String list(@RequestParam(name = "page", required = false, defaultValue = "1") Long page,
					   @RequestParam(name = "pageSize", required = false, defaultValue = "10") Long pageSize,
					   @RequestParam(name = "orderSn", required = false) String orderSn,
					   @RequestParam(name = "title", required = false) String title,
					   @RequestParam(name = "status", required = false) String status,
					   Model model) {
		QueryWrapper queryWrapper = new QueryWrapper();
		if(StrUtils.isNotBlank(orderSn)) {
			queryWrapper.eq("o.order_sn", orderSn);
		}
		if(StrUtils.isNotBlank(title)) {
			queryWrapper.like("o.order_title", title);
		}
		if(StrUtils.isNotBlank(status)) {
			queryWrapper.eq("o.pay_status", status);
		}
		queryWrapper.orderByDesc("o.created");
		model.addAttribute(PAGE_DATA_ATTR, orderService.pageOrder(new Page(page, pageSize), queryWrapper));
		return "admin/order/list";
	}

	@GetMapping("detail")
	public String detail(@RequestParam(name = "orderId") Long orderId,
						 Model model) {
		model.addAttribute("orderDetail", orderService.getOrderDetail(orderId));
		return "admin/order/detail";
	}

	@GetMapping("delivery")
	public String delivery(@RequestParam(name = "orderId") Long orderId,
						  Model model) {
		model.addAttribute("order", orderService.getById(orderId));
		model.addAttribute("express", expressCompService.list());
		model.addAttribute("delivery", orderService.getOrderDelivery(orderId));
		return "admin/order/delivery";
	}

	@PostMapping("doDelivery")
	public ResponseEntity doDelivery(@RequestParam(name = "orderId") Long orderId,
									 @RequestParam(name = "expName") String expName,
									 @RequestParam(name = "billNumber") String billNumber,
									 @RequestParam(name = "deliveryTime") String deliveryTime) {
		try {
			orderService.doOrderDelivery(orderId, expName, billNumber, deliveryTime);
			return Response.success();
		} catch (Exception e) {
			return Response.fail(e.getMessage());
		}
	}

	@GetMapping("remarks")
	public String remarks(@RequestParam(name = "orderId") Long orderId,
						  Model model) {
		model.addAttribute("order", orderService.getById(orderId));
		return "admin/order/remarks";
	}

	@PostMapping("doSaveRemarks")
	public ResponseEntity doSaveOrderRemarks(@RequestParam(name = "orderId") Long orderId,
											 @RequestParam(name = "remarks") String remarks) {

		Order order = orderService.getById(orderId);
		if(order == null) {
			return Response.fail("订单不存在");
		}

		order.setRemarks(remarks);
		orderService.updateById(order);

		return Response.success();
	}

	@GetMapping("changePrice")
	public String changePrice(@RequestParam(name = "orderId") Long orderId,
							  Model model) {
		model.addAttribute("order", orderService.getById(orderId));
		return "admin/order/price";
	}

	@PostMapping("doSavePrice")
	public ResponseEntity doSavePrice(@RequestParam(name = "orderId") Long orderId,
											 @RequestParam(name = "newPrice") String newPrice) {

		Order order = orderService.getById(orderId);
		if(order == null) {
			return Response.fail("订单不存在");
		}

		if(StrUtils.isBlank(newPrice)) {
			return Response.fail("价格不能为空");
		}
		order.setPayAmount(new BigDecimal(newPrice));
		orderService.updateById(order);

		return Response.success();
	}

}
