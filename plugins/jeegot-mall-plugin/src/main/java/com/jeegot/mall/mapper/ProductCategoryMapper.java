package com.jeegot.mall.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jeegot.aspect.DataPermission;
import com.jeegot.mall.entity.ProductCategory;
import com.jeegot.mall.service.IProductCategoryService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商品分类表 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface ProductCategoryMapper extends BaseMapper<ProductCategory> {

	void deleteRelationByCategoryId(Long productCategoryId);

	List<ProductCategory> getProductCategoryListByProductId(Long productId, String type);

	@DataPermission("a")
	Page<IProductCategoryService.ProductCategoryVo> pageProductCategory(Page pageParam, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

}
