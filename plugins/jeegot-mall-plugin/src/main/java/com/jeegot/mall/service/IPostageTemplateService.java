package com.jeegot.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jeegot.mall.entity.PostageTemplate;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 运费模板表 服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface IPostageTemplateService extends IService<PostageTemplate> {

	void savePostageTemplate(PostageTemplate postageTemplate) throws Exception;

	List<PostageTemplate> getUserPostageTemplate(Long userId);

	BigDecimal getPostageAmount(IOrderService.ProductParam productParam, String address);

	BigDecimal getPostageAmount(GetPostageAmountParam getPostageAmountParam);

	@Data
	class GetPostageAmountParam implements Serializable {
		String address;
		List<IOrderService.ProductParam> productParamList;
	}

}
