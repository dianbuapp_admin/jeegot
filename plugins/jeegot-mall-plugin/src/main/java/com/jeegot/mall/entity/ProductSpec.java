package com.jeegot.mall.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 商品规格表
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ProductSpec implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(fill = FieldFill.INSERT)
    private Long userId;

    /**
     * 规格名称
     */
    @NotBlank(message = "规格名称不能为空")
    private String specName;

    /**
     * 规格图标
     */
    private String specIcon;

    /**
     * 规格备注
     */
    private String specMemo;

    /**
     * 排序
     */
    private Integer sortNum;

    /**
     * 删除状态：1 正常 ，0 已经删除
     */
    private Integer status;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime created;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updated;

    @TableField(exist = false)
    private List<ProductSpecValue> specValues;

    @TableField(exist = false)
    private Long[] specValueIds;

    @TableField(exist = false)
    private String[] specValueNames;

    @TableField(exist = false)
    private Integer[] specValueSortNums;

}
