package com.jeegot.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeegot.mall.entity.ProductSkuItem;
import com.jeegot.mall.mapper.ProductSkuItemMapper;
import com.jeegot.mall.service.IProductSkuItemService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
@Service
public class ProductSkuItemServiceImpl extends ServiceImpl<ProductSkuItemMapper, ProductSkuItem> implements IProductSkuItemService {

	@Override
	public List<ProductSkuItem> getProductSkuItemList(Long productId) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("product_id", productId);
		return list(queryWrapper);
	}

	@Override
	public ProductSkuItem getProductSkuItem(Long productId, String skuName) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("product_id", productId);
		queryWrapper.eq("sku_name", skuName);
		return getOne(queryWrapper);
	}

}
