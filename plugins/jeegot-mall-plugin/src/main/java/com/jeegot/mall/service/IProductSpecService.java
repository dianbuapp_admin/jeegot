package com.jeegot.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jeegot.mall.entity.ProductSpec;

import java.util.List;

/**
 * <p>
 * 商品规格表 服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface IProductSpecService extends IService<ProductSpec> {

	List<ProductSpec> getUserProductSpecList(Long userId);

	void saveProductSpec(ProductSpec productSpec) throws Exception;

}
