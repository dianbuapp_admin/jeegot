/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.mall.listener;

import com.jeegot.core.payment.listener.PaymentSuccessListener;
import com.jeegot.entity.PaymentRecord;
import com.jeegot.mall.entity.Order;
import com.jeegot.mall.service.IOrderService;
import com.jeegot.service.IPaymentRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 处理商品订单支付完成逻辑
 * @author： wjun_java@163.com
 * @date： 2021/6/22
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Slf4j
@Component
public class OrderPaymentSuccessListener implements PaymentSuccessListener {

	@Autowired
	private IOrderService orderService;
	@Autowired
	private IPaymentRecordService paymentRecordService;

	@Override
	public void onSuccess(PaymentRecord paymentRecord) {
		if(paymentRecord.getTrxType().equals(PaymentRecord.TRX_TYPE_ORDER)) {
			//处理商城订单支付成逻辑
			paymentRecord.setPayStatus(Order.STATUS_PAY_SUCCESS);
			paymentRecordService.updateById(paymentRecord);
			Order order = orderService.getById(paymentRecord.getProductRelativeId());
			order.setPayStatus(Order.STATUS_PAY_SUCCESS);
			order.setDeliveryStatus(Order.DELIVERY_STATUS_UNDELIVERY);
			orderService.updateById(order);
		}
	}

}
