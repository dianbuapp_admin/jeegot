package com.jeegot.mall.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jeegot.mall.entity.ProductSpecValue;

import java.util.List;

/**
 * <p>
 * 规格值 服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
public interface IProductSpecValueService extends IService<ProductSpecValue> {

	List<ProductSpecValue> getSpecValueList(Long specId);

}
