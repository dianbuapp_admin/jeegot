package com.jeegot.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeegot.mall.entity.ProductSpec;
import com.jeegot.mall.entity.ProductSpecValue;
import com.jeegot.mall.mapper.ProductSpecMapper;
import com.jeegot.mall.service.IProductSpecService;
import com.jeegot.mall.service.IProductSpecValueService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品规格表 服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
@Service
public class ProductSpecServiceImpl extends ServiceImpl<ProductSpecMapper, ProductSpec> implements IProductSpecService {

	@Autowired
	private IProductSpecValueService productSpecValueService;

	@Override
	public List<ProductSpec> getUserProductSpecList(Long userId) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("user_id", userId);
		List<ProductSpec> productSpecList = list(queryWrapper);
		productSpecList.forEach(item -> {
			List<ProductSpecValue> productSpecValueList = productSpecValueService.getSpecValueList(item.getId());
			item.setSpecValues(productSpecValueList);
		});
		return productSpecList;
	}

	@Override
	@Transactional
	public void saveProductSpec(ProductSpec productSpec) throws Exception {
		Long specId = productSpec.getId();
		ProductSpec spec = getById(specId);
		if(spec == null){
			spec = new ProductSpec();
			spec.setStatus(1);
		}
		spec.setSpecName(productSpec.getSpecName());
		spec.setSortNum(productSpec.getSortNum());
		spec.setSpecMemo(productSpec.getSpecMemo());
		spec.setSpecIcon(productSpec.getSpecIcon());
		saveOrUpdate(spec);

		Long[] specValueIds = productSpec.getSpecValueIds();
		String[] specValueNames = productSpec.getSpecValueNames();
		Integer[] specValueOrderNums = productSpec.getSpecValueSortNums();

		List<ProductSpecValue> productSpecValueList = productSpecValueService.getSpecValueList(spec.getId());
		if(productSpecValueList != null && productSpecValueList.size()>0) {
			Map<String, Integer> tempMap = new HashMap<>();
			for(ProductSpecValue productSpecValue : productSpecValueList) {
				tempMap.put(String.valueOf(productSpecValue.getId()), 1);
			}
			for(int i=0; i<specValueIds.length; i++) {
				String key = specValueIds[i] == null ? null : specValueIds[i] + "";
				if(StringUtils.isNotBlank(key)) {
					if(tempMap.get(key) != null) {
						tempMap.put(key, tempMap.get(key) + 1);
					}else {
						tempMap.put(key, 1);
					}
				}
			}

			tempMap.keySet().forEach(item -> {
				if(null != tempMap.get(item) && tempMap.get(item) == 1) {
					productSpecValueService.removeById(item);
				}
			});

		}

		for(int i=0; i<specValueNames.length;i++) {
			ProductSpecValue productSpecValue = productSpecValueService.getById(specValueIds[i]);
			if(productSpecValue == null) {
				productSpecValue = new ProductSpecValue();
				productSpecValue.setSpecId(spec.getId());
			}

			productSpecValue.setSpecValueName(specValueNames[i]);
			productSpecValue.setSortNum(specValueOrderNums[i]);
			productSpecValueService.saveOrUpdate(productSpecValue);
		}
	}

}
