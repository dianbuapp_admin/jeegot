package com.jeegot.mall.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 运费设置
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PostageTemplateSet implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 运费模板id
     */
    private Long templateId;

    /**
     * 首N件
     */
    private Integer startStandards;

    /**
     * 首费(￥)
     */
    private BigDecimal startFees;

    /**
     * 续M件
     */
    private Integer addStandards;

    /**
     * 续费(￥)
     */
    private BigDecimal addFees;

    /**
     * 自定义地区运费设置
     */
    private String areaContent;

    /**
     * 是否默认运费设置
     */
    private Boolean isDefault;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime created;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updated;

}
