package com.jeegot.mall.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 发票信息表
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class OrderInvoice implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 发票类型
     */
    private String type;

    /**
     * 发票抬头
     */
    private String title;

    /**
     * 发票内容
     */
    private String content;

    /**
     * 纳税人识别号
     */
    private String identity;

    /**
     * 单位名称
     */
    private String name;

    /**
     * 发票收取人手机号
     */
    private String mobile;

    /**
     * 发票收取人邮箱
     */
    private String email;

    /**
     * 发票状态
     */
    private Integer status;

    /**
     * 修改时间
     */
    private LocalDateTime updated;

    /**
     * 创建时间
     */
    private LocalDateTime created;


}
