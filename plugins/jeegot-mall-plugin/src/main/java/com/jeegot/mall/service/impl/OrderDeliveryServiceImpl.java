package com.jeegot.mall.service.impl;

import com.jeegot.mall.entity.OrderDelivery;
import com.jeegot.mall.mapper.OrderDeliveryMapper;
import com.jeegot.mall.service.IOrderDeliveryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-06-14
 */
@Service
public class OrderDeliveryServiceImpl extends ServiceImpl<OrderDeliveryMapper, OrderDelivery> implements IOrderDeliveryService {

}
