package com.jeegot.coupon.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jeegot.coupon.entity.CouponCode;
import com.jeegot.coupon.service.ICouponCodeService;
import com.jeegot.coupon.service.ICouponService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 优惠券领取记录 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-10
 */
public interface CouponCodeMapper extends BaseMapper<CouponCode> {

	Page<ICouponCodeService.CouponCodeVo> pageCouponCode(Page page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

	List<ICouponService.CouponVo> getUserCoupon(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);

}
