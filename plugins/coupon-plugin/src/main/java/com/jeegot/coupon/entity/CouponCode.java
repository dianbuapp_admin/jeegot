package com.jeegot.coupon.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jeegot.core.plugin.UnLoad;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * <p>
 * 优惠券领取记录
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CouponCode implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 类型ID
     */
    private Long couponId;

    /**
     * 优惠券标题
     */
    private String title;

    /**
     * 优惠码
     */
    private String code;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 状态 1 有人领取、正常使用  2 未有人领取不能使用  3 已经使用，不能被再次使用  4 已经被认为标识不可用
     */
    private Integer status;

    /**
     * 领取时间
     */
    @TableField(fill =FieldFill.INSERT)
    private LocalDateTime validTime;

    /**
     * 创建时间，创建时可能不会有人领取
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime created;

    @UnLoad
    public enum CouponCodeStatus {

        M(1, "正常使用"),
        D(2, "未有人领取不能使用"),
        U(3, "已使用"),
        N(4, "不可用");

        CouponCodeStatus(Integer key, String value) {
            this.key = key;
            this.value = value;
        }

        private final Integer key;
        private final String value;

        public static String getValue(Integer key) {
            for (CouponCodeStatus s: values()) {
                if (Objects.equals(s.key, key)) {
                    return s.value;
                }
            }
            return "";
        }

    }

    public String getStatusStr() {
        return CouponCodeStatus.getValue(getStatus());
    }

    @TableField(exist = false)
    private Coupon coupon;


}
