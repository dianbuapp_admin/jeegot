package com.jeegot.coupon.service;

import com.jeegot.coupon.entity.CouponUsedRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠券使用记录 服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-10
 */
public interface ICouponUsedRecordService extends IService<CouponUsedRecord> {

}
