package com.jeegot.coupon.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jeegot.core.plugin.UnLoad;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * <p>
 * 优惠券
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Coupon implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 创建用户
     */
    @TableField(fill = FieldFill.INSERT)
    private Long userId;

    /**
     * 例如：无门槛50元优惠券 | 单品最高减2000元'
     */
    @NotBlank(message = "请输入优惠券名称")
    private String title;

    private String icon;

    private String description;

    /**
     * 1满减券  2叠加满减券  3无门槛券  
     */
    private Integer type;

    /**
     * 满多少金额
     */
    @NotNull(message = "请输入满减金额")
    private BigDecimal withAmount;

    /**
     * 会员可用
     */
    private Boolean withMember = false;

    /**
     * 是否是推广奖励券
     */
    private Boolean withAward = false;

    /**
     * 是不是只有领取人可用，如果不是，领取人可以随便给其他人用
     */
    private Boolean withOwner = true;

    /**
     * 是否可以多次使用
     */
    private Boolean withMulti = false;

    /**
     * 优惠券金额
     */
    @NotNull(message = "请输入优惠券金额")
    private BigDecimal amount;

    /**
     * 奖励金额，大咖可以使用自己的优惠码推广用户，用户获得优惠，大咖获得奖励金额
     */
    private BigDecimal awardAmount;

    /**
     * 配额：发券数量
     */
    @NotNull(message = "请输入优惠券数量")
    private Integer quota;

    /**
     * 已领取的优惠券数量
     */
    private Integer takeCount;

    /**
     * 已使用的优惠券数量
     */
    private Integer usedCount;

    /**
     * 发放开始时间
     */
    private LocalDateTime startTime;

    /**
     * 发放结束时间
     */
    private LocalDateTime endTime;

    /**
     * 时效:1绝对时效（XXX-XXX时间段有效）  2相对时效（领取后N天有效）
     */
    private Integer validType;

    /**
     * 使用开始时间
     */
    private LocalDateTime validStartTime;

    /**
     * 使用结束时间
     */
    private LocalDateTime validEndTime;

    /**
     * 自领取之日起有效天数
     */
    private Integer validDays;

    /**
     * 1 正常  9 不能使用
     */
    private Integer status;

    private String options;

    @TableField(fill =FieldFill.INSERT)
    private LocalDateTime created;

    @TableField(fill =FieldFill.INSERT_UPDATE)
    private LocalDateTime updated;

    @UnLoad
    public enum CouponType {

        M(1, "满减券"),
        D(2, "叠加满减券"),
        NONE(3, "无门槛券");

        CouponType(Integer key, String value) {
            this.key = key;
            this.value = value;
        }

        private final Integer key;
        private final String value;

        public static String getValue(Integer key) {
            for (CouponType s: values()) {
                if (Objects.equals(s.key, key)) {
                    return s.value;
                }
            }
            return "";
        }
    }

    public String getTypeStr() {
        return CouponType.getValue(getType());
    }


    @UnLoad
    public enum CouponStatus {

        M(1, "正常使用"),
        D(0, "失效");

        CouponStatus(Integer key, String value) {
            this.key = key;
            this.value = value;
        }

        private final Integer key;
        private final String value;

        public static String getValue(Integer key) {
            for (CouponStatus s: values()) {
                if (Objects.equals(s.key, key)) {
                    return s.value;
                }
            }
            return "";
        }

    }

    public String getStatusStr() {
        return CouponStatus.getValue(getStatus());
    }


}
