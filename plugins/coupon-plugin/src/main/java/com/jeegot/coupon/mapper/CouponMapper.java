package com.jeegot.coupon.mapper;

import com.jeegot.coupon.entity.Coupon;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jeegot.coupon.service.ICouponService;

import java.util.List;

/**
 * <p>
 * 优惠券 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-10
 */
public interface CouponMapper extends BaseMapper<Coupon> {

	List<ICouponService.CouponVo> getCouponList(Long userId);

}
