package com.jeegot.coupon.mapper;

import com.jeegot.coupon.entity.CouponUsedRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 优惠券使用记录 Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-10
 */
public interface CouponUsedRecordMapper extends BaseMapper<CouponUsedRecord> {

}
