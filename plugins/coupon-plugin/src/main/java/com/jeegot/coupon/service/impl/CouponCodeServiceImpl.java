package com.jeegot.coupon.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeegot.coupon.entity.CouponCode;
import com.jeegot.coupon.mapper.CouponCodeMapper;
import com.jeegot.coupon.service.ICouponCodeService;
import com.jeegot.coupon.service.ICouponService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 优惠券领取记录 服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-10
 */
@Service
public class CouponCodeServiceImpl extends ServiceImpl<CouponCodeMapper, CouponCode> implements ICouponCodeService {

	@Override
	public Page<CouponCodeVo> pageCouponCode(Page page, QueryWrapper queryWrapper) {
		return getBaseMapper().pageCouponCode(page, queryWrapper);
	}

	@Override
	public List<ICouponService.CouponVo> getUserCoupon(Long userId, Integer status) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq("cc.user_id", userId);
		if(status != null) {
			queryWrapper.eq("cc.status", status);
		}
		return getBaseMapper().getUserCoupon(queryWrapper);
	}

}
