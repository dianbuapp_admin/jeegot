/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.coupon.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jeegot.common.constants.JeegotConstants;
import com.jeegot.core.permission.AdminMenu;
import com.jeegot.core.response.Response;
import com.jeegot.coupon.entity.Coupon;
import com.jeegot.coupon.service.ICouponCodeService;
import com.jeegot.coupon.service.ICouponService;
import com.jeegot.web.controller.admin.AdminBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author： wjun_java@163.com
 * @date： 2021/7/9
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Controller
@AdminMenu(name = "卡券", icon = "<i class=\"nav-icon fas fa-credit-card\"></i>", sort = 1)
@RequestMapping(JeegotConstants.ADMIN_MAPPING + "/coupon")
public class CouponController extends AdminBaseController {

	@Autowired
	private ICouponService couponService;

	@Autowired
	private ICouponCodeService couponCodeService;

	@GetMapping("list")
	@AdminMenu(name = "优惠券", sort = 1)
	public String list(@RequestParam(name = "page", required = false, defaultValue = "1") Long page,
					   @RequestParam(name = "pageSize", required = false, defaultValue = "10") Long pageSize,
					   Model model) {
		Page<Coupon> couponPage = couponService.page(new Page<>(page, pageSize));
		model.addAttribute(PAGE_DATA_ATTR, couponPage);
		return "admin/coupon/list";
	}

	@AdminMenu(name = "新建", sort = 2)
	@RequestMapping("edit")
	public String edit(@RequestParam(name = "id", required = false) Long id, Model model) {
		model.addAttribute("coupon", couponService.getById(id));
		return "admin/coupon/edit";
	}

	@PostMapping("doSave")
	public ResponseEntity doSave(@Validated Coupon coupon) {
		if(coupon.getValidType() == 1) {
			//验证起始时间
			if(coupon.getValidStartTime() == null || coupon.getValidEndTime() == null) {
				return Response.fail("绝对时效不能为空");
			}
		}else if (coupon.getValidType() == 2){
			//验证有效天数
			if(coupon.getValidDays() == null) {
				return Response.fail("有效天数不能为空");
			}
		}
		couponService.saveOrUpdate(coupon);
		return Response.success();
	}

	@GetMapping("take")
	public String take(@RequestParam(name = "couponId") Long couponId,
					   Model model) {
		model.addAttribute(couponService.getById(couponId));
		return "admin/coupon/take";
	}

	@PostMapping("doTake")
	public ResponseEntity doTake(Long couponId, Long userId) {
		try {
			couponService.takeCoupon(couponId, userId);
			return Response.success();
		} catch (Exception e) {
			return Response.fail(e.getMessage());
		}
	}

	@AdminMenu(name = "领取记录", sort = 3)
	@GetMapping("takelist")
	public String takeList(@RequestParam(name = "page", required = false, defaultValue = "1") Long page,
						   @RequestParam(name = "pageSize", required = false, defaultValue = "10") Long pageSize,
						   @RequestParam(name = "couponId", required = false) Long couponId,
						   Model model) {
		QueryWrapper queryWrapper = new QueryWrapper();
		if(couponId != null) {
			queryWrapper.eq("cc.coupon_id", couponId);
		}
		Page<ICouponCodeService.CouponCodeVo> couponCodePage = couponCodeService.pageCouponCode(new Page<>(page, pageSize), queryWrapper);
		model.addAttribute(PAGE_DATA_ATTR, couponCodePage);
		return "admin/coupon/take_list";
	}

	@PostMapping("doDelTake")
	public ResponseEntity doDelTake(Long codeId) {
		return Response.success();
	}

}
