package com.jeegot.coupon.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 优惠券使用记录
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CouponUsedRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 使用优惠码的用户
     */
    private Long usedUserId;

    /**
     * 使用优惠码的用户ID
     */
    private String usedUserNickname;

    /**
     * 订单ID
     */
    private Long usedOrderId;

    /**
     * 支付的ID
     */
    private Long userPaymentId;

    /**
     * 优惠码ID
     */
    private Long codeId;

    /**
     * 优惠码名称
     */
    private String code;

    /**
     * 优惠券归属的用户ID
     */
    private Long codeUserId;

    /**
     * 优惠券归属的用户昵称
     */
    private String codeUserNickname;

    private Long couponId;

    /**
     * 使用时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime created;


}
