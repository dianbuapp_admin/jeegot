package com.jeegot.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jeegot.coupon.entity.Coupon;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 优惠券 服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-10
 */
public interface ICouponService extends IService<Coupon> {

	/**
	 * 用户领取优惠券
	 * @param couponId
	 * @param userId
	 * @throws Exception
	 */
	void takeCoupon(Long couponId, Long userId) throws Exception;

	/**
	 * 获取用户领取的优惠券
	 * @param userId
	 * @return
	 */
	List<CouponVo> getUserCoupon(Long userId, Integer status);

	/**
	 * 获取优惠券列表
	 * @param userId
	 * @return
	 */
	List<CouponVo> getCouponList(Long userId);

	@Data
	class CouponVo extends Coupon {

		/**
		 * 优惠码
		 */
		private String code;

		/**
		 * 状态 1 有人领取、正常使用  2 未有人领取不能使用  3 已经使用，不能被再次使用  4 已经被认为标识不可用
		 */
		private Integer status;

		/**
		 * 领取时间
		 */
		private LocalDateTime validTime;

		/**
		 * 领券人
		 */
		String taker;

		public boolean getIsTake() {
			return getTaker() != null;
		}
	}

}
