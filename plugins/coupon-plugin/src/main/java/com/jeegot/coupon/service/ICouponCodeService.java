package com.jeegot.coupon.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jeegot.coupon.entity.CouponCode;
import lombok.Data;

import java.util.List;

/**
 * <p>
 * 优惠券领取记录 服务类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-10
 */
public interface ICouponCodeService extends IService<CouponCode> {

	Page<CouponCodeVo> pageCouponCode(Page page, QueryWrapper queryWrapper);

	List<ICouponService.CouponVo> getUserCoupon(Long userId, Integer status);

	@Data
	class CouponCodeVo extends CouponCode {
		String nickName;
	}

}
