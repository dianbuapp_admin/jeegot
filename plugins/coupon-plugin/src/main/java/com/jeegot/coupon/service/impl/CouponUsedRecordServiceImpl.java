package com.jeegot.coupon.service.impl;

import com.jeegot.coupon.entity.CouponUsedRecord;
import com.jeegot.coupon.mapper.CouponUsedRecordMapper;
import com.jeegot.coupon.service.ICouponUsedRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券使用记录 服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-07-10
 */
@Service
public class CouponUsedRecordServiceImpl extends ServiceImpl<CouponUsedRecordMapper, CouponUsedRecord> implements ICouponUsedRecordService {

}
