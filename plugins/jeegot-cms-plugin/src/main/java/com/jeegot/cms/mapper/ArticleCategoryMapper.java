package com.jeegot.cms.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jeegot.aspect.DataPermission;
import com.jeegot.cms.entity.ArticleCategory;
import com.jeegot.cms.service.IArticleCategoryService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-05-23
 */
public interface ArticleCategoryMapper extends BaseMapper<ArticleCategory> {

	void deleteRelationByCategoryId(Long articleCategoryId);

	List<ArticleCategory> getArticleCategoryListByArticleId(Long articleId, String type);

	@DataPermission("a")
	Page<IArticleCategoryService.ArticleCategoryVo> pageArticleCategory(Page pageParam, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

}
