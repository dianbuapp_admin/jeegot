/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.cms.directive;

import com.jeegot.cms.entity.Article;
import com.jeegot.cms.entity.SinglePage;
import com.jeegot.common.constants.JeegotConstants;
import com.jeegot.core.directive.BaseFunction;
import com.jeegot.entity.Config;
import com.jeegot.service.IConfigService;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateModelException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author： wjun_java@163.com
 * @date： 2021/5/28
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Component("seoTag")
public class SeoDirective extends BaseFunction {

	@Autowired
	private IConfigService configService;

	@Override
	public Object exec(List arguments) throws TemplateModelException {

		SimpleScalar simpleScalar = (SimpleScalar) arguments.get(0);
		String key = simpleScalar.getAsString();
		if(StringUtils.isBlank(key)) return "";

		HttpServletRequest request = getRequest();
		SinglePage singlePage = (SinglePage) request.getAttribute("singlePage");
		if(singlePage != null) {
			if(JeegotConstants.WEBSITE_TITLE_KEY.equals(key.trim())) {
				return singlePage.getSeoKeywords();
			}

			if(JeegotConstants.WEBSITE_SUB_TITLE_KEY.equals(key.trim())) {
				return singlePage.getSeoDescription();
			}
		}

		Article article = (Article) request.getAttribute("article");
		if(article != null) {
			if(JeegotConstants.WEBSITE_TITLE_KEY.equals(key.trim())) {
				return article.getSeoKeywords();
			}

			if(JeegotConstants.WEBSITE_SUB_TITLE_KEY.equals(key.trim())) {
				return article.getSeoDescription();
			}
		}

		Config config = configService.findByKey(key);
		return config == null ? "" : config.getValue();
	}

}
