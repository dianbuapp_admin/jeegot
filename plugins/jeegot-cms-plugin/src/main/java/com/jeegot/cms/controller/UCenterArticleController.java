/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.cms.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jeegot.cms.entity.Article;
import com.jeegot.cms.service.IArticleCategoryService;
import com.jeegot.cms.service.IArticleCommentService;
import com.jeegot.cms.service.IArticleService;
import com.jeegot.common.constants.JeegotConstants;
import com.jeegot.core.permission.UcenterMenu;
import com.jeegot.core.response.Response;
import com.jeegot.core.utils.CaptchaUtils;
import com.jeegot.web.controller.ucenter.UCenterBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author： wjun_java@163.com
 * @date： 2021/6/4
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Controller
@RequestMapping(JeegotConstants.UCENTER_MAPPING + "/article")
@UcenterMenu(name = "文章")
public class UCenterArticleController extends UCenterBaseController {

	@Autowired
	private IArticleService articleService;

	@Autowired
	private IArticleCategoryService articleCategoryService;

	@Autowired
	private IArticleCommentService articleCommentService;

	@RequestMapping("list")
	@UcenterMenu(name = "文章列表", sort = 1)
	public String list(@RequestParam(name = "page", required = false, defaultValue = "1") Long page,
					   @RequestParam(name = "pageSize", required = false, defaultValue = "10") Long pageSize,
					   @RequestParam(name = "title", required = false) String title,
					   @RequestParam(name = "status", required = false) String status,
					   Model model) {
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.eq(USER_ID, getLoginUser().getId());
		Page<Article> pageData = articleService.page(new Page<>(page, pageSize), queryWrapper);
		model.addAttribute(PAGE_DATA_ATTR, pageData);
		return "ucenter/article/list";
	}

	@RequestMapping("write")
	@UcenterMenu(name = "投稿", sort = 2)
	public String write(@RequestParam(name = "id", required = false) Long id, Model model) {
		model.addAttribute("article", articleService.getById(id));
		model.addAttribute("tags", articleCategoryService.getArticleTagListByArticleId(id));
		model.addAttribute("tagList", articleCategoryService.getTagList(getLoginUser().getId()));
		model.addAttribute("categories", articleCategoryService.getArticleCategoryListByArticleId(id));
		model.addAttribute("categoryList", articleCategoryService.getCategoryList(getLoginUser().getId()));
		return "ucenter/article/edit";
	}

	@PostMapping("doSave")
	public ResponseEntity doSave(@Validated Article article) {
		try {
			if(article.getId() == null) {
				article.setStatus(Article.STATUS_AUDIT);
			}
			articleService.saveArticle(article);
			return Response.success();
		} catch (Exception e) {
			return Response.fail(e.getMessage());
		}
	}

	@PostMapping("doSaveComment")
	public ResponseEntity doSaveComment(Long articleId, Long commentId, String context, String captcha) {
		if(!CaptchaUtils.checkCaptcha(captcha)) {
			return Response.fail("验证码输入错误");
		}

		try {
			articleCommentService.saveArticleComment(articleId, commentId, context);
			return Response.success();
		} catch (Exception e) {
			return Response.fail(e.getMessage());
		}

	}

}
