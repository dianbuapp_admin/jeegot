package com.jeegot.cms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jeegot.cms.entity.SinglePageComment;
import com.jeegot.cms.mapper.SinglePageCommentMapper;
import com.jeegot.cms.service.ISinglePageCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 页面评论表 服务实现类
 * </p>
 *
 * @author wjun_java@163.com
 * @since 2021-05-25
 */
@Service
public class SinglePageCommentServiceImpl extends ServiceImpl<SinglePageCommentMapper, SinglePageComment> implements ISinglePageCommentService {

	@Override
	public Page<SinglePageCommentMapper.PageCommentVo> pageSinglePageComment(Page pageParam, QueryWrapper queryWrapper) {
		return getBaseMapper().pageSinglePageComment(pageParam, queryWrapper);
	}
}
