/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.web.listener;

import com.jeegot.core.permission.PermissionManager;
import com.jeegot.core.plugin.PluginManagerService;
import com.jeegot.web.JeegotWebApplication;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * 应用程序启动完成监听事件
 * @author： wjun_java@163.com
 * @date： 2021/2/19
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Slf4j
@Component
public class JeegotAppStartedListener implements ApplicationListener<ApplicationStartedEvent> {

    @Autowired
    private PluginManagerService pluginManagerService;

    @Autowired
    private PermissionManager permissionManager;

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {

        try {
            permissionManager.initSystemPermissions(JeegotWebApplication.class);
            pluginManagerService.initPlugins();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

    }

}
