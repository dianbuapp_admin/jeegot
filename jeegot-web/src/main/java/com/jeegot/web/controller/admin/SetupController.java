/**
 * Copyright (c) 广州小橘灯信息科技有限公司 2016-2017, wjun_java@163.com.
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * http://www.xjd2020.com
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jeegot.web.controller.admin;

import com.jeegot.common.constants.JeegotConstants;
import com.jeegot.core.permission.AdminMenu;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author： wjun_java@163.com
 * @date： 2021/6/6
 * @description：
 * @modifiedBy：
 * @version: 1.0
 */
@Controller
@AdminMenu(name = "设置", icon = "<i class=\"nav-icon fa fa-cog\"></i>", sort = 5)
@RequestMapping(JeegotConstants.ADMIN_MAPPING + "/setup")
public class SetupController extends AdminBaseController {

	@AdminMenu(name = "小程序", sort = 1)
	@RequestMapping("wechatMini")
	public String wechatMini() {
		return "/admin/setup/wechat_mini";
	}

	@AdminMenu(name = "支付", sort = 2)
	@RequestMapping("pay")
	public String pay() {
		return "/admin/setup/pay";
	}

	@RequestMapping("icons")
	public String icons() {
		return "/admin/setup/icons";
	}

}
