package com.jeegot.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.jeegot")
public class JeegotWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(JeegotWebApplication.class, args);
    }

}
